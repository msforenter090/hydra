#include "byte_array_test.h"

#include "hydra.platform.detection/type.h"
#include "hydra.asn/byte_array.h"

namespace hydra {
    namespace asn {

        const unsigned long testDataLength = 16;
        platform::type::byte8 zeroTestData[] = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
        platform::type::byte8 testData[] = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F";

        byte_array createTestByteArray() {
            return byte_array{ testDataLength, testData };
        }

        namespace test {
            void byte_array_test::SetUp() {}

            void byte_array_test::TearDown() {}

            void checkLengthAndData(const byte_array& byteArray, const platform::type::byte8* const data, const unsigned long length) {
                ASSERT_EQ(length, byteArray.length());
                ASSERT_NE(nullptr, byteArray.data());

                // All elements are present from test data
                for(unsigned long i = 0; i < byteArray.length(); ++i) {
                    ASSERT_EQ(data[i], byteArray[i]);
                }
            }

            TEST_F(byte_array_test, ByteArrayLengthCtor) {
                const byte_array byteArrayWithLength{ 16 };
                checkLengthAndData(byteArrayWithLength, zeroTestData, testDataLength);

                const byte_array byteArrayNoLength{ 0 };
                checkLengthAndData(byteArrayNoLength, zeroTestData, 0);
            }

            TEST_F(byte_array_test, ByteArrayLengthDataCtor) {
                byte_array byteArrayWithLength = createTestByteArray();
                checkLengthAndData(byteArrayWithLength, testData, testDataLength);
            }

            TEST_F(byte_array_test, ByteArrayCopyCtor) {
                byte_array byteArrayWithLength = createTestByteArray();
                byte_array byteArrayWithLengthCopy{ byteArrayWithLength };
                checkLengthAndData(byteArrayWithLengthCopy, testData, testDataLength);
            }

            TEST_F(byte_array_test, ByteArrayMoveCtor) {
                byte_array byteArrayWithLength = createTestByteArray();
                byte_array byteArrayWithLengthCopy{ std::move(byteArrayWithLength) };

                ASSERT_EQ(0, byteArrayWithLength.length());
                ASSERT_EQ(nullptr, byteArrayWithLength.data());

                checkLengthAndData(byteArrayWithLengthCopy, testData, testDataLength);
            }

            TEST_F(byte_array_test, ByteArrayAssignmentOperator) {
                byte_array byteArrayWithLength = createTestByteArray();
                byte_array byteArrayWithLengthZero{ 0 };

                // use assignment operator
                byteArrayWithLengthZero = byteArrayWithLength;

                // check if boath instances have sam value
                checkLengthAndData(byteArrayWithLength, testData, testDataLength);
                checkLengthAndData(byteArrayWithLengthZero, testData, testDataLength);

                // check if thay are sharing memory (they should not), change one and check against test data
                byteArrayWithLength[0] = '\xFF';
                platform::type::byte8 changedData[testDataLength];
                memcpy(changedData, testData, testDataLength);
                changedData[0] = '\xFF';
                checkLengthAndData(byteArrayWithLength, changedData, testDataLength);
                checkLengthAndData(byteArrayWithLengthZero, testData, testDataLength);
            }

            TEST_F(byte_array_test, ByteArrayMoveAssignmentOperator) {
                byte_array byteArrayWithLengthZero{ 0 };
                byte_array byteArrayWithLength = createTestByteArray();

                checkLengthAndData(byteArrayWithLengthZero, zeroTestData, 0);

                // Move assignment operator, should leave rhs value in null state and lhs has new state (copied from rhs)
                byteArrayWithLengthZero = std::move(byteArrayWithLength);

                // New state for rhs is null
                ASSERT_EQ(0, byteArrayWithLength.length());
                ASSERT_EQ(nullptr, byteArrayWithLength.data());

                // New state for lhs is moved from rhs
                checkLengthAndData(byteArrayWithLengthZero, testData, testDataLength);
            }

            TEST_F(byte_array_test, ByteArrayLength) {
                const byte_array byteArrayWithLength = createTestByteArray();
                ASSERT_EQ(testDataLength, byteArrayWithLength.length());
            }

            TEST_F(byte_array_test, ByteArrayData) {
                const byte_array byteArrayWithLengthZero{ 0 };
                const byte_array byteArrayWithLength = createTestByteArray();

                ASSERT_NE(nullptr, byteArrayWithLengthZero.data());
                ASSERT_NE(nullptr, byteArrayWithLength.data());
            }

            TEST_F(byte_array_test, ByteArrayOperatorBrackets) {
                const byte_array byteArrayWithLength = createTestByteArray();

                // Iterate over array and check values against values from test array
                for(unsigned long i = 0; i < byteArrayWithLength.length(); ++i) {
                    ASSERT_EQ(testData[i], byteArrayWithLength[i]);
                }
            }

            TEST_F(byte_array_test, ByteArrayIsAllocated) {
                const byte_array byteArrayWithLength = createTestByteArray();
                const byte_array byteArrayWithLengthZero{ 0 };

                ASSERT_TRUE(byteArrayWithLength.isAllocated());
                ASSERT_FALSE(byteArrayWithLengthZero.isAllocated());
            }
        }
    }
}
