#pragma once

#include "gtest/gtest.h"

namespace hydra {
    namespace asn {
        namespace test {
            class byte_array_test : public ::testing::Test {
            protected:
                void SetUp() override;
                void TearDown() override;
            };
        }
    }
}