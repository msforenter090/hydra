#include "HydraFactory.h"
#include "Hydra.h"

bool hydra::HydraFactory::CreateHydra(IHydra ** Hydra, hydra::HydraOutputWindow& OutputWindow, HydraEngineSelector EngineSelector) const noexcept {
    if(Hydra == nullptr)
        return false;

    try {
        *Hydra = new hydra::Hydra(OutputWindow, EngineSelector);
    } catch(...) {
        *Hydra = nullptr;
        return false;
    }

    return true;
}

bool hydra::HydraFactory::DestroyHydra(IHydra ** Hydra) const noexcept {
    if(Hydra == nullptr || *Hydra == nullptr)
        return true;

    delete *Hydra;
    *Hydra = nullptr;

    return true;
}
