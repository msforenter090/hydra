#include "Hydra.h"

//#include "hydra.core/HydraCore.h"

#include <memory>

// Pimpl Implementation Start
class hydra::Hydra::HydraImplementation {
private:
    //std::unique_ptr<hydra::core::HydraCore> m_HydraCore;
public:
    HydraImplementation(HydraOutputWindow& OutputWindow, HydraEngineSelector EngineSelector) {
        // TODO: Use a factory method.
        //hydra::core::HydraCoreOutputWindow HydraCoreOutputWindow = HydraOutputWindowToHydraCoreOutputWindow(EngineSelector, OutputWindow);
        //hydra::core::HydraCoreEngineSelector HydraCoreEngineSelector = HydraEngineSelectorToHydraCoreEngineSelector(EngineSelector);
        //m_HydraCore.reset(new hydra::core::HydraCore(HydraCoreOutputWindow, HydraCoreEngineSelector));
    }

    //hydra::core::HydraCoreEngineSelector HydraEngineSelectorToHydraCoreEngineSelector(HydraEngineSelector EngineSelector) {
    //    switch(EngineSelector) {
    //        case HydraEngineSelector::VULKAN:
    //            return hydra::core::HydraCoreEngineSelector::VULKAN;
    //        case HydraEngineSelector::DIRECT_X:
    //            return hydra::core::HydraCoreEngineSelector::DIRECT_X;
    //        default:
    //            return hydra::core::HydraCoreEngineSelector::VULKAN;;
    //    }
    //}

    //hydra::core::HydraCoreOutputWindow HydraOutputWindowToHydraCoreOutputWindow(HydraEngineSelector EngineSelector, HydraOutputWindow& HydraOutputWindow) {
    //#ifdef _WIN32
    //    switch(EngineSelector) {
    //        case HydraEngineSelector::VULKAN: {
    //            hydra::core::HydraCoreWindowsOutputWindow HydraCoreWindowsWindow(HydraOutputWindow.GetWindowsOutputWindow().GetGLDeviceContext(),
    //                                                                             HydraOutputWindow.GetWindowsOutputWindow().GetGLRenderContext());
    //            return hydra::core::HydraCoreOutputWindow(HydraCoreWindowsWindow);
    //        }
    //        case HydraEngineSelector::DIRECT_X: {
    //            hydra::core::HydraCoreWindowsOutputWindow HydraCoreWindowsWindow(HydraOutputWindow.GetWindowsOutputWindow().GetWindowHandle());
    //            return hydra::core::HydraCoreOutputWindow(HydraCoreWindowsWindow);
    //        }
    //        default: {
    //            // TODO: Unpredicatable error. (Exit or exception)
    //            hydra::core::HydraCoreWindowsOutputWindow HydraCoreWindowsWindow(nullptr, nullptr);
    //            return hydra::core::HydraCoreOutputWindow(HydraCoreWindowsWindow);
    //            break;
    //        }
    //    }
    //#elif defined __linux__
    //    hydra::core::HydraCoreLinuxOutputWindow LinuxOutputWindow;
    //    return hydra::core::HydraCoreOutputWindow(LinuxOutputWindow);
    //#else
    //    #error Unknown platform.
    //#endif
    //}

    void Frame() const noexcept {
        // TODO Core Frame or Draw.
    }
};

// Pimple Implementation End

hydra::Hydra::Hydra(HydraOutputWindow& OutputWindow, HydraEngineSelector EngineSelector)
    : m_Implementation(new HydraImplementation(OutputWindow, EngineSelector)) {
}

hydra::Hydra::~Hydra() = default;

void hydra::Hydra::Frame() const noexcept {
    m_Implementation->Frame();
}