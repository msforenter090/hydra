#pragma once

#include "Exports.h"
#include "IHydra.h"

#include "HydraOutputWindow.h"
#include "HydraEngineSelector.h"

namespace hydra {
    class HYDRA_EXPORT Hydra : public IHydra {
    private:
        class HydraImplementation;
        HydraImplementation* m_Implementation;

    public:
        Hydra(HydraOutputWindow& OutputWindow, HydraEngineSelector EngineSelector);
        virtual ~Hydra() noexcept;

        void Frame() const noexcept override;
    };
}