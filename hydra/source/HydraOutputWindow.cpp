#include "HydraOutputWindow.h"

#ifdef _WIN32
    hydra::HydraOutputWindow::HydraOutputWindow(HydraWindowsOutputWindow WindowsOutputWindow) noexcept
        : m_WindowsOutputWindow(WindowsOutputWindow) {
    }

    hydra::HydraWindowsOutputWindow hydra::HydraOutputWindow::GetWindowsOutputWindow() const noexcept {
        return m_WindowsOutputWindow;
    }
#elif defined(__linux__)
    hydra::HydraOutputWindow::HydraOutputWindow(HydraLinuxOutputWindow LinuxOutputWindow) noexcept
        : m_LinuxOutputWindow(LinuxOutputWindow) {
    }

    hydra::HydraLinuxOutputWindow hydra::HydraOutputWindow::GetLinuxOutputWindow() const noexcept {
        return m_LinuxOutputWindow;
    }
#endif