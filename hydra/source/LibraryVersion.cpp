#include "LibraryVersion.h"

unsigned int CreateVersion(const char major, const char minor, const char patch) noexcept {
    return major << 24 | minor << 16 | patch << 8;
}

unsigned int hydra::version::GetVersion() noexcept {
    return CreateVersion(HYDRA_VERSION_MAJOR, HYDRA_VERSION_MINOR, HYDRA_VERSION_PATCH);
}
