#include "HydraWindowsOutputWindow.h"

#ifdef _WIN32
hydra::HydraWindowsOutputWindow::HydraWindowsOutputWindow(HDC* const hdc, HGLRC* const hglrc) noexcept
    : m_DeviceContextHandle(hdc), m_GLRenderContextHandle(hglrc) {
}

hydra::HydraWindowsOutputWindow::HydraWindowsOutputWindow(HWND const hwnd) noexcept
    : m_WindowHandle(hwnd) {
}

HDC* hydra::HydraWindowsOutputWindow::GetGLDeviceContext() noexcept {
    return m_DeviceContextHandle;
}

HGLRC* hydra::HydraWindowsOutputWindow::GetGLRenderContext() noexcept {
    return m_GLRenderContextHandle;
}

HWND hydra::HydraWindowsOutputWindow::GetWindowHandle() noexcept {
    return m_WindowHandle;
}
#endif