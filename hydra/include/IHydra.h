#pragma once

#include "Exports.h"

namespace hydra {
    class HYDRA_EXPORT IHydra {
    public:
        IHydra();
        virtual void Frame() const noexcept = 0;
        virtual ~IHydra();
    };
}