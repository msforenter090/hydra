#pragma once

#include "Exports.h"

namespace hydra {
    namespace version {
        /** \briefly:   Function returnes version of the library encoded into unsigned int.
        *               From left to right
        *               1st byte (MSB) holds the major version,
        *               2nd byte holds the minor version
        *               3rd byte (LSB) holds patch
        *               4th byte is not used and it is zero.
        */
        HYDRA_EXPORT unsigned int GetVersion() noexcept;
    }
}