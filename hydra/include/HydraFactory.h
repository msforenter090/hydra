#pragma once

#include "Exports.h"
#include "IHydra.h"
#include "HydraOutputWindow.h"
#include "HydraEngineSelector.h"

namespace hydra {
    class HYDRA_EXPORT HydraFactory {
    public:
        /** \briefly:   Creates Hydra instance.
        *               Multiple calls are allowed.
        *               First call will create the instance.
        *               Others will return same instance as the first.
        *   \param[in]: IHydra, Must be a non null value else false is returned.
        *   \param[in]: HydraOutputWindow, Render target. Library will use this window to draw content.
        *   \param[in]: HydraEngineSelector, Indicates which engine to use for rendering.
        *   \return:    If creation is successful true is returned false otherwise.
        */
        bool CreateHydra(IHydra** Hydra, hydra::HydraOutputWindow& OutputWindow, HydraEngineSelector EngineSelector) const noexcept;

        /** \briefly:   Destroys previously created instance of Hydra.
        *               Multiple calls are allowed. First call will dealocate resources and return true.
        *               Each other call will return false.
        *   \param[in]: Hydra, Prevously created Hydra. Must be non null value else false is returned.
        *   \return:    If dealocation was successful true is returned false otherwise.
        */
        bool DestroyHydra(IHydra** Hydra) const noexcept;
    };
}