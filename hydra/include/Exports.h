#pragma once

#ifdef HYDRA_EXPORT_API
    #ifdef WIN32
        #define HYDRA_EXPORT __declspec(dllexport)
    #elif defined __linux__
        #define HYDRA_EXPORT
    #endif
#else
    #ifdef WIN32
        #define HYDRA_EXPORT __declspec(dllimport)
    #elif defined __linux__
        #define HYDRA_EXPORT
    #endif
#endif