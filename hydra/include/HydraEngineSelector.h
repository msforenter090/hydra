#pragma once

namespace hydra {
    enum class HydraEngineSelector : short {
        VULKAN = 0,
        DIRECT_X
    };
}