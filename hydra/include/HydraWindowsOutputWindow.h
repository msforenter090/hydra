#pragma once

#include "Exports.h"
#ifdef _WIN32
#include <Windows.h>

namespace hydra {
    class HYDRA_EXPORT HydraWindowsOutputWindow {
    private:
        // OpenGL Window
        HDC* m_DeviceContextHandle;
        HGLRC* m_GLRenderContextHandle;

        // Win32 Window
        HWND m_WindowHandle;

    public:
        HydraWindowsOutputWindow(HDC* const hdc, HGLRC* const hglrc) noexcept;
        HydraWindowsOutputWindow(HWND const hwnd) noexcept;

        HDC* GetGLDeviceContext() noexcept;
        HGLRC* GetGLRenderContext() noexcept;

        HWND GetWindowHandle() noexcept;
    };
}
#endif // _WIN32