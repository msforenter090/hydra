#pragma once

#include "Exports.h"

#ifdef _WIN32
    #include "HydraWindowsOutputWindow.h"
#elif defined __linux__
    #include "HydraLinuxOutputWindow.h"
#else
    #error Unknown platform.
#endif

namespace hydra {
    class HYDRA_EXPORT HydraOutputWindow {
    private:
    #ifdef _WIN32
        hydra::HydraWindowsOutputWindow m_WindowsOutputWindow;
    #elif defined __linux__
        hydra::HydraLinuxOutputWindow m_LinuxOutputWindow;
    #else
        #error Unknown platform.
    #endif

    public:
    #if defined(_WIN32)
        HydraOutputWindow(hydra::HydraWindowsOutputWindow WindowsOutputWindow) noexcept;
        HydraWindowsOutputWindow GetWindowsOutputWindow() const noexcept;
    #elif defined(__linux__)
        HydraOutputWindow(HydraLinuxOutputWindow LinuxOutputWindow) noexcept;
        HydraLinuxOutputWindow GetLinuxOutputWindow() const noexcept;
    #else
        #error Unknown platform.
    #endif
    };
}