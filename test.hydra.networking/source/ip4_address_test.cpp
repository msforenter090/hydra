#include "ip4_address_test.h"

#include "hydra.networking/ip4_address.h"

namespace hydra {
    namespace network {
        namespace test {
            void ip4_address_test::SetUp() {}

            void ip4_address_test::TearDown() {}

            TEST_F(ip4_address_test, test_empty_ctor) {

                // ip4 address without args should set address to localhost:8080
                hydra::network::ip4_address address;
                ASSERT_TRUE(0 == strcmp("127.0.0.1", address.address())) << "ip4_address_test, test_empty_ctor, ip address does not match expected value";
                ASSERT_EQ(8080, address.port()) << "ip4_address_test, test_empty_ctor, ip address port does not match expected value";
            }

            TEST_F(ip4_address_test, test_invalid_ctor) {
                // invalid ip address, missing port
                hydra::network::ip4_address invalid_address_1{"193.168.2.10"};
                ASSERT_TRUE(0 == strcmp("127.0.0.1", invalid_address_1.address())) << "ip4_address_test, test_invalid_ctor, ip address does not match expected value";
                ASSERT_EQ(8080, invalid_address_1.port()) << "ip4_address_test, test_invalid_ctor, ip address port does not match expected value";

                // invalid ip address, missing ip address
                hydra::network::ip4_address invalid_address_2{ ":132" };
                ASSERT_TRUE(0 == strcmp("127.0.0.1", invalid_address_2.address())) << "ip4_address_test, test_invalid_ctor, ip address does not match expected value";
                ASSERT_EQ(8080, invalid_address_2.port()) << "ip4_address_test, test_invalid_ctor, ip address port does not match expected value";

                // invalid ip address, missing address octet
                hydra::network::ip4_address invalid_address_3{ "193.168.2:5050" };
                ASSERT_TRUE(0 == strcmp("127.0.0.1", invalid_address_3.address())) << "ip4_address_test, test_invalid_ctor, ip address does not match expected value";
                ASSERT_EQ(8080, invalid_address_3.port()) << "ip4_address_test, test_invalid_ctor, ip address port does not match expected value";

                // invalid ip address, ip address octer out of range
                hydra::network::ip4_address invalid_address_4{ "888.255.15.33:5050" };
                ASSERT_TRUE(0 == strcmp("127.0.0.1", invalid_address_4.address())) << "ip4_address_test, test_invalid_ctor, ip address does not match expected value";
                ASSERT_EQ(8080, invalid_address_4.port()) << "ip4_address_test, test_invalid_ctor, ip address port does not match expected value";

                // invalid ip address, port out of range
                hydra::network::ip4_address invalid_address_5{ "193.255.15.33:71000" };
                ASSERT_TRUE(0 == strcmp("127.0.0.1", invalid_address_5.address())) << "ip4_address_test, test_invalid_ctor, ip address does not match expected value";
                ASSERT_EQ(8080, invalid_address_5.port()) << "ip4_address_test, test_invalid_ctor, ip address port does not match expected value";
            }

            TEST_F(ip4_address_test, test_valid_ctor) {
                // valid ip address
                hydra::network::ip4_address invalid_address_1{ "127.0.0.1:23000" };
                ASSERT_TRUE(0 == strcmp("127.0.0.1", invalid_address_1.address())) << "ip4_address_test, test_valid_ctor, ip address does not match expected value";
                ASSERT_EQ(23000, invalid_address_1.port()) << "ip4_address_test, test_valid_ctor, ip address port does not match expected value";
            }
        }
    }
}
