#include "network_message_test.h"

#include "hydra.networking/network_message.h"

namespace hydra {
    namespace network {
        namespace test {
            void network_message_test::SetUp() {}
            void network_message_test::TearDown() {}

            hydra::platform::type::byte8 data[] = {
                0x00, 0x01, 0x02, 0x03,
                0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0A, 0x0B,
                0x0C, 0x0D, 0x0E, 0x0F
            };

            hydra::platform::type::byte8 data_row_2[] = {
                0x04, 0x05, 0x06, 0x07
            };

            void check_data(const byte8* const data, network_message& message) {
                for(ushort16 i = 0; i < message.length(); i++) {
                    ASSERT_EQ(data[i], message.data()[i]) << "byte not expected, at index: " << i;
                }
            }

            TEST_F(network_message_test, empty_ctor) {
                network_message message;
                ASSERT_EQ(0, message.length()) << "network_message_test, empty_ctor, length does not have expected value.";
                ASSERT_NE(nullptr, message.data()) << "network_message_test, empty_ctor, data does not have expected value.";
                ASSERT_EQ(network::NETWORK_ARRAY_MAX_LENGTH, message.remaining()) << "network_message_test, empty_ctor, remaining does not have expected value.";
            }

            TEST_F(network_message_test, data_ctor) {
                // Create network byte message, starting from 4th byte, 4 byte long.
                network_message message{4, data, 4};
                ASSERT_EQ(4, message.length()) << "network_message_test, data_ctor, length does not have expected value.";
                ASSERT_NE(nullptr, message.data()) << "network_message_test, data_ctor, data does not have expected value.";
                ASSERT_EQ(network::NETWORK_ARRAY_MAX_LENGTH - 4, message.remaining()) << "network_message_test, data_ctor, remaining does not have expected value.";
                check_data(data_row_2, message);
            }

            TEST_F(network_message_test, copy_ctor) {
                // Create network byte message, starting from 4th byte, 4 byte long.
                network_message message{ 4, data, 4 };
                ASSERT_EQ(4, message.length()) << "network_message_test, data_ctor, length does not have expected value.";
                ASSERT_NE(nullptr, message.data()) << "network_message_test, data_ctor, data does not have expected value.";
                ASSERT_EQ(network::NETWORK_ARRAY_MAX_LENGTH - 4, message.remaining()) << "network_message_test, data_ctor, remaining does not have expected value.";
                
                // create a copy of the message and compare it with init data.
                network_message copy{message};
                ASSERT_EQ(4, copy.length()) << "network_message_test, data_ctor, length does not have expected value.";
                ASSERT_NE(nullptr, copy.data()) << "network_message_test, data_ctor, data does not have expected value.";
                ASSERT_EQ(network::NETWORK_ARRAY_MAX_LENGTH - 4, copy.remaining()) << "network_message_test, data_ctor, remaining does not have expected value.";

                check_data(data_row_2, copy);
            }

            TEST_F(network_message_test, assignment_operator) {
                // Create network byte message, starting from 4th byte, 4 byte long.
                network_message message{ 4, data, 4 };
                ASSERT_EQ(4, message.length()) << "network_message_test, data_ctor, length does not have expected value.";
                ASSERT_NE(nullptr, message.data()) << "network_message_test, data_ctor, data does not have expected value.";
                ASSERT_EQ(network::NETWORK_ARRAY_MAX_LENGTH - 4, message.remaining()) << "network_message_test, data_ctor, remaining does not have expected value.";

                // create a copy of the message and compare it with init data.
                network_message copy;
                copy = message;
                ASSERT_EQ(4, copy.length()) << "network_message_test, data_ctor, length does not have expected value.";
                ASSERT_NE(nullptr, copy.data()) << "network_message_test, data_ctor, data does not have expected value.";
                ASSERT_EQ(network::NETWORK_ARRAY_MAX_LENGTH - 4, copy.remaining()) << "network_message_test, data_ctor, remaining does not have expected value.";

                check_data(data_row_2, copy);
            }
        }
    }
}