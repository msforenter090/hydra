#include "network_server_test.h"

#include <thread>
#include <iostream>

#include "hydra.networking/ip4_address.h"
#include "hydra.networking/network_server.h"
#include "hydra.networking/network_manager.h"

namespace hydra {
    namespace network {
        namespace test {
            void network_server_test::SetUp() {}
            void network_server_test::TearDown() {}

            TEST_F(network_server_test, test_network_server) {
                network_manager network_manager;

                // Should initialize sockets for use.
                ASSERT_TRUE(network_manager.initialized());

                // Make sure this port is open and not used.
                ip4_address server_localhost("127.0.0.1:23000");

                std::function<void(client_socket)> callback_1 = [](client_socket client) -> void {
                    std::cout << "New Client." << std::endl;
                };
                std::function<void(void)> callback_2 = []() -> void {
                    std::cout << "Client droped." << std::endl;
                };

                network_server server(server_localhost, &callback_1, &callback_2);

                ASSERT_TRUE(server.obtain_address());
                ASSERT_TRUE(server.create_socket());
                ASSERT_TRUE(server.bind_socket());
            }
        }
    }
}