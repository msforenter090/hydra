#pragma once

#include "gtest/gtest.h"

namespace hydra {
    namespace network {
        namespace test {
            class network_server_test : public ::testing::Test {
            protected:
                void SetUp() override;
                void TearDown() override;
            };
        }
    }
}