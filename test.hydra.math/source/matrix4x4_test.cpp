#include "matrix4x4_test.h"

#include <cstring>
#include "hydra.math/matrix_arithmetics.h"

namespace hydra {
    namespace math {
        namespace test {
            void matrix4x4_test::SetUp() {}
            void matrix4x4_test::TearDown() {}

            const float matrix_1_data[] = {  1,  2,  3,  4,
                                             5,  6,  7,  8,
                                             9, 10, 11, 12,
                                            13, 14, 15, 16 };

            const float matrix_1_transpose_data[] = {   1,  5,  9, 13,
                                                        2,  6, 10, 14,
                                                        3,  7, 11, 15,
                                                        4,  8, 12, 16 };

            const float matrix_2_data[] = { 17, 18, 19, 20,
                                            21, 22, 23, 24,
                                            25, 26, 27, 28,
                                            29, 30, 31, 32 };

            const float matrix_addition_data[] = {  18, 20, 22, 24,
                                                    26, 28, 30, 32,
                                                    34, 36, 38, 40,
                                                    42, 44, 46, 48 };

            const float matrix_substraction_data[] = {  -16, -16, -16, -16,
                                                        -16, -16, -16, -16,
                                                        -16, -16, -16, -16,
                                                        -16, -16, -16, -16 };

            const float matrix_multiplication_data[] = {     250,  260,  270,  280,
                                                             618,  644,  670,  696,
                                                             986, 1028, 1070, 1112,
                                                            1354, 1412, 1470, 1528 };

            /// <summary> Checks if matrix elements are matching expected values.</summary>
            /// <param name="expected_data">Expected values in the matrix.</param>
            /// <param name="matrix">Matrix to inspect against expeted data.</param>
            void checkMatrixValues(const float* const expected_data, const matrix::matrix4x4& matrix) {
                ASSERT_EQ(0, memcmp(expected_data, matrix.mData, matrix::MATRIX4x4_LENGTH));
            }

            /// <summary>Test matrix initialization. All elements are initialized with same value.</summary>
            TEST_F(matrix4x4_test, initialize) {
                matrix::matrix4x4 matrix;

                // Initialize matrix. All elements inside matrix have same value.
                float value = 10;
                matrix::initialize(matrix, value);
                float expected_value[] = {  value, value, value, value,
                                            value, value, value, value,
                                            value, value, value, value,
                                            value, value, value, value };
                checkMatrixValues(expected_value, matrix);
            }

            /// <summary>Test matrix initialization. All diagonal elements are initialized with same value, rest is zero</summary>
            TEST_F(matrix4x4_test, diagonal_initialize) {
                matrix::matrix4x4 matrix;

                // Diagonal initialize matrix. All elements on main diagonal have same value.
                // Rest are zeros.
                float value = 10;
                matrix::diagonal_initialize(matrix, value);
                float expected_value[] = {  value, 0, 0, 0,
                                            0, value, 0, 0,
                                            0, 0, value, 0,
                                            0, 0, 0, value };
                checkMatrixValues(expected_value, matrix);
            }

            TEST_F(matrix4x4_test, initialize_with_data) {
                matrix::matrix4x4 matrix;

                // Initialize matrix using array data.
                matrix::initialize_with_data(matrix, matrix_1_data);

                // Check matrix values. Must match with ones from matrix initialization array.
                checkMatrixValues(matrix_1_data, matrix);
            }

            /// <summary>Test matrix copy.</summary>
            TEST_F(matrix4x4_test, copy) {
                matrix::matrix4x4 original;
                matrix::matrix4x4 copy;

                // Diagonal initialize matrix. All elements on main diagonal have same value.
                // Rest are zeros.
                float original_value = 10;
                float copy_value = 20;

                matrix::diagonal_initialize(original, original_value);
                matrix::diagonal_initialize(copy, copy_value);
                matrix::copy(copy, original);
                float expected_value_original[] = { original_value, 0, 0, 0,
                                                    0, original_value, 0, 0,
                                                    0, 0, original_value, 0,
                                                    0, 0, 0, original_value };
                float expected_value_copy[] = { copy_value, 0, 0, 0,
                                                0, copy_value, 0, 0,
                                                0, 0, copy_value, 0,
                                                0, 0, 0, copy_value };

                // Original value should not be changed
                checkMatrixValues(expected_value_original, original);

                // Copy should have changed
                checkMatrixValues(expected_value_original, copy);
            }

            /// <summary>Test matrix addition.</summary>
            TEST_F(matrix4x4_test, add_new) {
                matrix::matrix4x4 lhs, rhs;

                // Initialize both matrices
                matrix::initialize_with_data(lhs, matrix_1_data);
                matrix::initialize_with_data(rhs, matrix_2_data);

                // Add them to produce new matrix. lhs and rhs matrices must stay unchanged.
                matrix::matrix4x4 result = matrix::add_new(lhs, rhs);

                // Check result.
                checkMatrixValues(matrix_addition_data, result);

                // Check operands.
                checkMatrixValues(matrix_1_data, lhs);
                checkMatrixValues(matrix_2_data, rhs);
            }

            /// <summary>Test matrix addition.</summary>
            TEST_F(matrix4x4_test, add) {
                matrix::matrix4x4 lhs, rhs;

                // Initialize both matrices
                matrix::initialize_with_data(lhs, matrix_1_data);
                matrix::initialize_with_data(rhs, matrix_2_data);

                // Add them to produce new matrix. lhs should have changed. lhs holds the result.
                matrix::matrix4x4& result = matrix::add(lhs, rhs);

                // Check result.
                checkMatrixValues(matrix_addition_data, lhs);

                // Result should be reference to lhs. rhs should be unchanged.
                ASSERT_EQ(&result, &lhs);
                checkMatrixValues(matrix_2_data, rhs);
            }

            /// <summary>Test matrix substraction.</summary>
            TEST_F(matrix4x4_test, substract_new) {
                matrix::matrix4x4 lhs, rhs;

                // Initialize both matrices
                matrix::initialize_with_data(lhs, matrix_1_data);
                matrix::initialize_with_data(rhs, matrix_2_data);

                // Add them to produce new matrix. lhs and rhs matrices must stay unchanged.
                matrix::matrix4x4 result = matrix::substract_new(lhs, rhs);

                // Check result.
                checkMatrixValues(matrix_substraction_data, result);

                // Check operands.
                checkMatrixValues(matrix_1_data, lhs);
                checkMatrixValues(matrix_2_data, rhs);
            }

            /// <summary>Test matrix substraction.</summary>
            TEST_F(matrix4x4_test, substract) {
                matrix::matrix4x4 lhs, rhs;

                // Initialize both matrices
                matrix::initialize_with_data(lhs, matrix_1_data);
                matrix::initialize_with_data(rhs, matrix_2_data);

                // Add them to produce new matrix. lhs should have changed. lhs holds the result.
                matrix::matrix4x4& result = matrix::substract(lhs, rhs);

                // Check result.
                checkMatrixValues(matrix_substraction_data, lhs);

                // Result should be reference to lhs. rhs should be unchanged.
                ASSERT_EQ(&result, &lhs);
                checkMatrixValues(matrix_2_data, rhs);
            }

            /// <summary>Test matrix multiplication.</summary>
            TEST_F(matrix4x4_test, multiply_new) {
                matrix::matrix4x4 lhs, rhs;

                // Initialize both matrices
                matrix::initialize_with_data(lhs, matrix_1_data);
                matrix::initialize_with_data(rhs, matrix_2_data);

                // Add them to produce new matrix. lhs and rhs matrices must stay unchanged.
                matrix::matrix4x4 result = matrix::multiply_new(lhs, rhs);

                // Check result.
                checkMatrixValues(matrix_multiplication_data, result);

                // Check operands.
                checkMatrixValues(matrix_1_data, lhs);
                checkMatrixValues(matrix_2_data, rhs);
            }

            /// <summary>Test matrix multiplication.</summary>
            TEST_F(matrix4x4_test, multiply) {
                matrix::matrix4x4 lhs, rhs;

                // Initialize both matrices
                matrix::initialize_with_data(lhs, matrix_1_data);
                matrix::initialize_with_data(rhs, matrix_2_data);

                // Add them to produce new matrix. lhs should have changed. lhs holds the result.
                matrix::matrix4x4& result = matrix::multiply(lhs, rhs);

                // Check result.
                checkMatrixValues(matrix_multiplication_data, lhs);

                // Result should be reference to lhs. rhs should be unchanged.
                ASSERT_EQ(&result, &lhs);
                checkMatrixValues(matrix_2_data, rhs);
            }

            /// <summary>Test matrix transpose.</summary>
            TEST_F(matrix4x4_test, transpose_new) {
                matrix::matrix4x4 transposition;

                // Initialize both matrices
                matrix::initialize_with_data(transposition, matrix_1_data);

                // Add them to produce new matrix. lhs and rhs matrices must stay unchanged.
                matrix::matrix4x4 result = matrix::transpose_new(transposition);

                // Check result.
                checkMatrixValues(matrix_1_transpose_data, result);

                // Check operands.
                checkMatrixValues(matrix_1_data, transposition);
                checkMatrixValues(matrix_1_transpose_data, result);
            }

            /// <summary>Test matrix transpose.</summary>
            TEST_F(matrix4x4_test, transpose) {
                matrix::matrix4x4 transposition;

                // Initialize both matrices
                matrix::initialize_with_data(transposition, matrix_1_data);

                // Add them to produce new matrix. lhs should have changed. lhs holds the result.
                matrix::matrix4x4& result = matrix::transpose(transposition);

                // Check result.
                checkMatrixValues(matrix_1_transpose_data, result);

                // Result should be reference to lhs. rhs should be unchanged.
                ASSERT_EQ(&result, &transposition);
            }
        }
    }
}
