#include "vector4_test.h"

#include "hydra.math/vector_arithmetics.h"

namespace hydra {
    namespace math {
        namespace test {
            void vector4_test::SetUp() {}
            void vector4_test::TearDown() {}

            void checkvector4Elements(const vector::vector4& vector, const float el1, const float el2, const float el3, const float el4) {
                ASSERT_EQ(el1, vector.mData[0]);
                ASSERT_EQ(el2, vector.mData[1]);
                ASSERT_EQ(el3, vector.mData[2]);
                ASSERT_EQ(el4, vector.mData[3]);
            }

            /// <summary>Test if initialization function works correctly.</summary>
            TEST_F(vector4_test, initialize_single_value) {
                vector::vector4 position;

                // Initialize all elelments with values.
                vector::initialize(position, 1);
                checkvector4Elements(position, 1, 1, 1, 1);
            }

            TEST_F(vector4_test, initialize_multiple_values) {
                vector::vector4 position;

                // Initialize all elelments with one values.
                vector::initialize(position, 1, 2, 3, 4);
                checkvector4Elements(position, 1, 2, 3, 4);
            }

            /// <summary>Test vector copy.</summary>
            TEST_F(vector4_test, copy) {
                vector::vector4 position;

                // Initialize all elelments with one values.
                vector::initialize(position, 1, 2, 3, 4);
                checkvector4Elements(position, 1, 2, 3, 4);

                vector::vector4 position_copy;
                // Copy to new vector and check values
                vector::copy(position_copy, position);
                checkvector4Elements(position_copy, 1, 2, 3, 4);

                // Change original vector and recheck vector copy (make sure memory is not shared between instances).
                position.mData[0] = 5;
                checkvector4Elements(position, 5, 2, 3, 4);
                checkvector4Elements(position_copy, 1, 2, 3, 4);
            }

            /// <summary>Test vector addition.</summary>
            TEST_F(vector4_test, add) {
                vector::vector4 vector1;
                vector::vector4 vector2;

                // Initialize all elelments with one values.
                // add_new
                vector::initialize(vector1, 1, 2, 3, 4);
                vector::initialize(vector2, 5, 6, 7, 8);
                vector::vector4 result = vector::add_new(vector1, vector2);
                checkvector4Elements(result, 6, 8, 10, 12);

                // add
                add(vector1, vector2);
                // vector1 is the output vector
                checkvector4Elements(vector1, 6, 8, 10, 12);

                // vector2 must stay unchanged
                vector::initialize(vector2, 5, 6, 7, 8);
            }

            /// <summary>Test vector substraction.</summary>
            TEST_F(vector4_test, substract) {
                vector::vector4 vector1;
                vector::vector4 vector2;

                // Initialize all elelments with one values.
                // substract_new
                vector::initialize(vector1, 1, 2, 3, 4);
                vector::initialize(vector2, 5, 6, 7, 8);
                vector::vector4 result = vector::substract_new(vector1, vector2);
                checkvector4Elements(result, -4, -4, -4, -4);

                // substract
                substract(vector1, vector2);
                // vector1 is the output vector
                checkvector4Elements(vector1, -4, -4, -4, -4);

                // vector2 must stay unchanged
                vector::initialize(vector2, 5, 6, 7, 8);
            }

            /// <summary>Test vector magnitude.</summary>
            TEST_F(vector4_test, magnitude) {
                vector::vector4 vector1;
                vector::vector4 vector2;

                // Initialize all elelments with one values.
                // substract_new
                vector::initialize(vector1, 1, 2, 3, 4);
                vector::initialize(vector2, 0, 0, 0, 0);

                // TODO: number epsilon surounding
                // sqrt (30)
                float magnitude_vector1 = magnitude(vector1);
                ASSERT_EQ(0, magnitude(vector2));
            }
        }
    }
}