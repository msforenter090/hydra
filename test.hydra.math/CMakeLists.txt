cmake_minimum_required(VERSION ${CMAKE_MIN_VERSION})

message(STATUS "########################################################################################")
message(STATUS "#                                    test.hydra.math                                   #")
message(STATUS "########################################################################################")
message(STATUS "Using CMake Min Version:" ${CMAKE_MIN_VERSION})

set(TARGET_NAME test.hydra.math)
project(${TARGET_NAME})

set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)
set(VERSION_PATCH 0)
set(VERSION_STRING ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

include_directories(include/)
SET(HEADERS     "include/vector4_test.h"
                "include/matrix4x4_test.h")
SET(SOURCES     "source/vector4_test.cpp"
                "source/matrix4x4_test.cpp"
                "source/main.cpp")

############################################ Create target Start ############################################
# If Linux executable name is:      test.hydra.math
# If Windows executable name is:    test.hydra.math
add_executable(${TARGET_NAME} ${SOURCES} ${HEADERS})
set_target_properties(${TARGET_NAME} PROPERTIES
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS OFF
)
############################################  Create target End  ############################################

##################################### Set target additional flags Start #####################################
# Skip rpath setting in library
set(CMAKE_SKIP_RPATH ON)
###################################### Set target additional flags End ######################################

######################################## Set output directory Start  ########################################
get_output_bin_folder_path(output_bin_path_debug Debug)
get_output_bin_folder_path(output_bin_path_release Release)
is_debug(debug)

if(debug EQUAL 1)
    get_output_bin_folder_path(make_file_output_bin_path Debug)
else(debug EQUAL 1)
    get_output_bin_folder_path(make_file_output_bin_path Release)
endif(debug EQUAL 1)

message(STATUS ${TARGET_NAME} ": MSVC Output bin path (Debug):      " ${output_bin_path_debug})
message(STATUS ${TARGET_NAME} ": MSVC Output bin path (Release):    " ${output_bin_path_release})
message(STATUS ${TARGET_NAME} ": Make Output bin path:              " ${make_file_output_bin_path})

# This is output for build type oriented tools, like MSVC
if(MSVC)
set_target_properties(${TARGET_NAME}
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY_DEBUG "${output_bin_path_debug}"
    RUNTIME_OUTPUT_DIRECTORY_RELEASE "${output_bin_path_release}"
)
else(MSVC)
# Output directory for makefile
set_target_properties(${TARGET_NAME}
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${make_file_output_bin_path}"
)
endif(MSVC)
######################################## Set output directory End ###########################################

############################## Add shared / output include directory Start ##################################
get_shared_include_folder_path(shared_include_path)
get_output_include_folder_path(output_include_path)
message(STATUS ${TARGET_NAME} ": Shared include path:               " ${shared_include_path})
message(STATUS ${TARGET_NAME} ": Output include path:               " ${output_include_path})
include_directories(${shared_include_path})
include_directories(${output_include_path})
############################## Add shared / output include directory End ####################################

############################# Add shared / output library directory Start  ##################################
get_shared_bin_folder_path(shared_bin_path_debug Debug)
get_shared_bin_folder_path(shared_bin_path_release Release)

message(STATUS ${TARGET_NAME} ": Shared bin path (Debug):           " ${shared_bin_path_debug})
message(STATUS ${TARGET_NAME} ": Shared bin path (Release):         " ${shared_bin_path_release})
# hydra.math library has no binaries, only headers. that is why there is no output bin path

if(MSVC)
    target_link_libraries(${TARGET_NAME}
        debug ${output_bin_path_debug}/gtest.lib
        optimized ${output_bin_path_release}/gtest.lib)
else(MSVC)
    if(debug EQUAL 1)
        target_link_libraries(${TARGET_NAME}
            ${output_bin_path_debug}/libgtest.a pthread)
    else(debug EQUAL 1)
        target_link_libraries(${TARGET_NAME}
            ${output_bin_path_release}/libgtest.a pthread)
    endif(debug EQUAL 1)
endif(MSVC)
################################### Add shared library directory End  #######################################
