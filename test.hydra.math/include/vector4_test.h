#pragma once

#include "gtest/gtest.h"

namespace hydra {
    namespace math {
        namespace test {
            class vector4_test : public ::testing::Test {
            protected:
                void SetUp() override;
                void TearDown() override;
            };
        }
    }
}