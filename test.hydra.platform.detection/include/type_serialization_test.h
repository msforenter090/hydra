#pragma once

#include "gtest/gtest.h"

namespace hydra {
    namespace platform {
        namespace test {
            class type_serialization_test : public ::testing::Test {
            protected:
                void SetUp() override;
                void TearDown() override;
            };
        }
    }
}