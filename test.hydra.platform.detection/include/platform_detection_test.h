#pragma once

#include "gtest/gtest.h"

namespace hydra {
    namespace platform {
        namespace test {
            class platform_detection_test : public ::testing::Test {
            protected:
                void SetUp() override;
                void TearDown() override;
            };
        }
    }
}