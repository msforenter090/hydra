#include "type_serialization_test.h"

#include "hydra.platform.detection/type.h"
#include "hydra.platform.detection/serialization.h"

#ifdef _H_WINDOWS
#include <vld.h>
#endif // _H_WINDOWS

using namespace hydra::platform;
using namespace hydra::platform::type;

namespace hydra {
    namespace platform {
        namespace test {
            void type_serialization_test::SetUp() {}
            void type_serialization_test::TearDown() {}

            TEST_F(type_serialization_test, serialize_deserialize_int) {

                // serialize / deserialize unsigned int
                uint32 u_value = 0xFFFFFFFF;
                ubyte8 u_serialized[sizeof(uint32)];                        // 4 bytes array

                serialize<uint32>(u_value, u_serialized, 0);
                ASSERT_EQ(u_value, deserialize<uint32>(u_serialized, 0));

                // serialize / deserialize int
                int32 value = 0x0FFFFFFF;
                ubyte8 serialized[sizeof(int32)];

                serialize<int32>(value, serialized, 0);
                ASSERT_EQ(value, deserialize<int32>(serialized, 0));
            }

            TEST_F(type_serialization_test, serialize_deserialize_ptr) {
                // serialize / deserialize buffer
                uint32* value = (uint32*)malloc(sizeof(uint32));
                *value = 101;
                ubyte8 serialized[sizeof(auto_size_32_64)];

                serialize<auto_size_32_64>(reinterpret_cast<auto_size_32_64>(value), serialized, 0);
                value = nullptr;
                value = reinterpret_cast<uint32*>(deserialize<auto_size_32_64>(serialized, 0));

                ASSERT_EQ(101, 101);
                free(value);
            }
        }
    }
}