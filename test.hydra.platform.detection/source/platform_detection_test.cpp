#include "platform_detection_test.h"

#include "hydra.platform.detection/type.h"
#include "hydra.platform.detection/platform.h"

namespace hydra {
    namespace platform {
        namespace test {
            void platform_detection_test::SetUp() {}
            void platform_detection_test::TearDown() {}

            TEST_F(platform_detection_test, type_sizes) {
                ASSERT_TRUE(check_platform_types()) << "Platform types do not match expected values.";
            }

            TEST_F(platform_detection_test, platform_funcion) {
            #if defined _WIN32
                ASSERT_TRUE(windows());
                ASSERT_FALSE(linux());
            #elif defined __linux__
                ASSERT_FALSE(windows());
                ASSERT_TRUE(linux());
            #endif // _WIN32
            }

            TEST_F(platform_detection_test, macro) {

                // If windows platform (indicated by _MSC_VER) _H_WINDOWS should be defined and _H_LINUX should not be defined.
            #if defined _MSC_VER && (!defined _H_WINDOWS || defined _H_LINUX)
                FAIL();
            #endif

                // If ubuntu platform (indicated by __linux__) _H_LINUX should be defined and _H_WINDOWS should not be defined.
            #if defined __linux__ && (!defined _H_LINUX || defined _H_WINDOWS)
                FAIL();
            #endif

                // If Windows and 32bit build _H_BIN32 should be defined and _H_BIN64 should not be defined.
            #if defined _MSC_VER && defined _WIN32 && !defined _WIN64 && (!defined _H_BIN32 || defined _H_BIN64)
                FAIL();
            #endif

                // If Windows and 64bit build _H_BIN32 should not be defined and _H_BIN64 should be defined.
            #if defined _MSC_VER && defined _WIN32 && defined _WIN64 && (defined _H_BIN32 || !defined _H_BIN64)
                FAIL();
            #endif
            }
        }
    }
}