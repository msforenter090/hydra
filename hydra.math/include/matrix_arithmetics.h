#pragma once

// std::fill_n
#include <algorithm>

#include "array_arithmetics.h"

#include "matrix.h"

namespace hydra {
    namespace math {
        namespace matrix {

            ////////////////////////////////////////////
            //          matrix4x4 Arithmetics         //
            ////////////////////////////////////////////

            /// <summary>Initializes matrix with given value. All elements have ${rhs} value.</summary>
            /// <param name="lhs">Matrix to initialize.</param>
            /// <param name="value">Value to initialize with.</param>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& initialize(matrix4x4& lhs, const float value) noexcept {
                std::fill_n(lhs.mData, MATRIX4x4_LENGTH, value);
                return lhs;
            }

            /// <summary>Initializes matrix diagonally. Main diagonal has value ${rhs}, resit are zeros.</summary>
            /// <param name="lhs">Matrix to initialize.</param>
            /// <param name="value">Value to initialize with.</param>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& diagonal_initialize(matrix4x4& lhs, const float value) noexcept {
                // fill with zeros
                std::fill_n(lhs.mData, MATRIX4x4_LENGTH, static_cast<float>(0));

                // set main diagonal
                lhs.mData[0] = lhs.mData[5] = lhs.mData[10] = lhs.mData[15] = value;
                return lhs;
            }

            /// <summary>Initializes matrix with passed in data.</summary>
            /// <param name="lhs">Matrix to initialize.</param>
            /// <param name="data">Values to initialize with.</param>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& initialize_with_data(matrix4x4& lhs, const float* const data) noexcept {
                std::memcpy(lhs.mData, data, matrix::MATRIX4x4_LENGTH * sizeof(float));
                return lhs;
            }

            /// <summary>Copy from rhs to lhs.</summary>
            /// <param name="lhs">Matrix to copy into.</param>
            /// <param name="rhs">Matrix to copy from.</param>
            /// <returns>Returns: Matrix. Matrix is reference to lhs.</returns>
            inline matrix4x4& copy(matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                std::copy(rhs.mData, rhs.mData + MATRIX4x4_LENGTH, lhs.mData);
                return lhs;
            }

            /// <summary>Adds two matrices and produces the third as a result.
            ///          add_new, new is from new matrix as a result.</summary>
            /// <param name="lhs">Left hand side matrix.</param>
            /// <param name="rhs">Right hand side matrix.</param>
            /// <returns>Returns: matrix4x4. New matrix4x4 is result of addition.</returns>
            inline matrix4x4 add_new(const matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                matrix4x4 result;
                // Add row 1
                array::add_array_4(result.mData, lhs.mData, rhs.mData);

                // Add row 2, pointer arithmetics
                array::add_array_4(result.mData + 4, lhs.mData + 4, rhs.mData + 4);

                // Add row 3
                array::add_array_4(result.mData + 8, lhs.mData + 8, rhs.mData + 8);

                // Add row 4
                array::add_array_4(result.mData + 12, lhs.mData + 12, rhs.mData + 12);
                return result;
            }

            /// <summary>Adds two matrixes, result is stored in lhs, rhs is unchanged.</summary>
            /// <param name="lhs">Left hand side matrix.</param>
            /// <param name="rhs">Right hand side matrix.</param>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& add(matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                matrix4x4 result = add_new(lhs, rhs);
                return copy(lhs, result);
            }

            /// <summary>Substracts two matrices and produces the third as a result.
            ///          substract_new, new is from new matrix as a result.</summary>
            /// <param name="lhs">Left hand side matrix.</param>
            /// <param name="rhs">Right hand side matrix.</param>
            /// <returns>Returns: matrix4x4. New matrix4x4 is result of substraction.</returns>
            inline matrix4x4 substract_new(const matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                matrix4x4 result;
                // Add row 1
                array::sub_array_4(result.mData, lhs.mData, rhs.mData);

                // Add row 2, pointer arithmetics
                array::sub_array_4(result.mData + 4, lhs.mData + 4, rhs.mData + 4);

                // Add row 3
                array::sub_array_4(result.mData + 8, lhs.mData + 8, rhs.mData + 8);

                // Add row 4
                array::sub_array_4(result.mData + 12, lhs.mData + 12, rhs.mData + 12);
                return result;
            }

            /// <summary>Substracts two matrixes, result is stored in lhs, rhs is unchanged.</summary>
            /// <param name="lhs">Left hand side matrix.</param>
            /// <param name="rhs">Right hand side matrix.</param>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& substract(matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                matrix4x4 tmp = substract_new(lhs, rhs);
                return copy(lhs, tmp);
            }

            // linear combination:
            // a[0] * B.row[0] + a[1] * B.row[1] + a[2] * B.row[2] + a[3] * B.row[3]
            inline __m128 lincomb(const __m128 &lhs, const matrix4x4 &matrix) {
                __m128 result;
                result = _mm_mul_ps(_mm_shuffle_ps(lhs, lhs, 0x00), _mm_load_ps(matrix.mData));
                result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(lhs, lhs, 0x55), _mm_load_ps(matrix.mData + 4)));
                result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(lhs, lhs, 0xaa), _mm_load_ps(matrix.mData + 8)));
                result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(lhs, lhs, 0xff), _mm_load_ps(matrix.mData + 12)));
                return result;
            }

            /// <summary>Multiplies two matrices and produces the third as a result.
            ///          mulpiply_new, new is from new matrix as a result.</summary>
            /// <param name="lhs">Left hand side matrix.</param>
            /// <param name="rhs">Right hand side matrix.</param>
            /// <returns>Returns: matrix4x4. New matrix4x4 is result of multiplication.</returns>
            matrix4x4 multiply_new(const matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                matrix4x4 result;

                // result_ij = sum_k lhs_ik rhs_kj
                // => result_0j = lhs_00 * rhs_0j + lhs_01 * rhs_1j + lhs_02 * rhs_2j + lhs_03 * rhs_3j
                __m128 out0x = lincomb(_mm_load_ps(lhs.mData), rhs);
                __m128 out1x = lincomb(_mm_load_ps(lhs.mData + 4), rhs);
                __m128 out2x = lincomb(_mm_load_ps(lhs.mData + 8), rhs);
                __m128 out3x = lincomb(_mm_load_ps(lhs.mData + 12), rhs);

                _mm_store_ps(result.mData, out0x);
                _mm_store_ps(result.mData + 4, out1x);
                _mm_store_ps(result.mData + 8, out2x);
                _mm_store_ps(result.mData + 12, out3x);

                return result;
            }

            /// <summary>Multiplies two matrices and produces the third as a result.
            ///          mulpiply_new, new is from new matrix as a result.</summary>
            /// <param name="lhs">Left hand side matrix.</param>
            /// <param name="rhs">Right hand side matrix.</param>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& multiply(matrix4x4& lhs, const matrix4x4& rhs) noexcept {
                matrix4x4 tmp = multiply_new(lhs, rhs);
                copy(lhs, tmp);
                return lhs;
            }

            /// <summary>Transposes input matrix and produces new as a result.</summary>
            /// <param name="lhs">Matrix to transpose.</param>
            /// <returns>Returns: matrix4x4. New transposed matrix.</returns>
            inline matrix4x4 transpose_new(const matrix4x4& lhs) noexcept {
                // Do not initialize, it will be initialized by copy.
                matrix4x4 result;
                copy(result, lhs);

                // Main diagonal is properly initialized with previous copy.
                // Now transpose rest of the elements.
                // Matrix is fixed size, so iteration is not needed.
                result.mData[1] = lhs.mData[4];
                result.mData[2] = lhs.mData[8];
                result.mData[3] = lhs.mData[12];
                result.mData[6] = lhs.mData[9];
                result.mData[7] = lhs.mData[13];
                result.mData[11] = lhs.mData[14];

                return result;
            }

            /// <summary>Transposes input matrix and returns changed matrix as result.</summary>
            /// <param name="lhs">Matrix to transpose.</param>
            /// <returns>Returns: matrix4x4. New transposed matrix.</returns>
            /// <returns>Returns: matrix4x4. matrix4x4 is reference to lhs.</returns>
            inline matrix4x4& transpose(matrix4x4& lhs) noexcept {
                matrix4x4 tmp = transpose_new(lhs);
                return copy(lhs, tmp);
            }
        }
    }
}
