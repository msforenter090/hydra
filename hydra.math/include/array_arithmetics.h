#pragma once

// SSE / SSE 2
#include "xmmintrin.h"

namespace hydra {
    namespace math {
        namespace array {

            /// <summary>Swap values in two passed in variables.</summary>
            /// <param name="lhs">Left hand side value.</param>
            /// <param name="rhs">Right hand side value.</param>
            template<typename T>
            inline void swap(T& lhs, T& rhs) noexcept {
                T tmp = lhs;
                lhs = rhs;
                rhs = tmp;
            }

            /// <summary>Adds two array of length 4.
            /// Here different implementations of array can be implemented, other than sse / sse2</summary>
            /// <param name="out">Output array.</param>
            /// <param name="lhs">Left hand side array.</param>
            /// <param name="rhs">Right hand side array.</param>
            inline void add_array_4(float* const out, const float* const lhs, const float* const rhs) noexcept {
                _mm_store_ps(out, _mm_add_ps(_mm_load_ps(lhs), _mm_load_ps(rhs)));
            }

            /// <summary>Substracts two array of length 4.
            /// Here different implementations of array can be implemented, other than sse / sse2</summary>
            /// <param name="out">Output array.</param>
            /// <param name="lhs">Left hand side array.</param>
            /// <param name="rhs">Right hand side array.</param>
            inline void sub_array_4(float* const out, const float* const lhs, const float* const rhs) noexcept {
                _mm_store_ps(out, _mm_sub_ps(_mm_load_ps(lhs), _mm_load_ps(rhs)));
            }
        }
    }
}