#pragma once

// std::fill_n
#include <algorithm>

// sqrt
#include <math.h>

#include "array_arithmetics.h"

#include "vector.h"

namespace hydra {
    namespace math {
        namespace vector {

            ////////////////////////////////////////////
            //           vector4 Arithmetics          //
            ////////////////////////////////////////////

            /// <summary>Initializes vector with given value.</summary>
            /// <param name="lhs">Vector to initialize.</param>
            /// <param name="rhs">Value to initialize with.</param>
            /// <returns>Returns: vector4. vector4 is reference to lhs.</returns>
            inline vector4& initialize(vector4& lhs, const float value) noexcept {
                std::fill_n(lhs.mData, VECTOR4_LENGTH, value);
                return lhs;
            }

            /// <summary>Initializes vector with given values.</summary>
            /// <param name="lhs">Vector to initialize.</param>
            /// <param name="value1">x component of vector.</param>
            /// <param name="value2">y component of vector.</param>
            /// <param name="value3">z component of vector.</param>
            /// <param name="value4">w component of vector.</param>
            /// <returns>Returns: vector4. vector4 is reference to lhs.</returns>
            inline vector4& initialize(vector4& lhs, const float value1, const float value2, const float value3, const float value4) noexcept {
                lhs.mData[0] = value1;
                lhs.mData[1] = value2;
                lhs.mData[2] = value3;
                lhs.mData[3] = value4;
                return lhs;
            }

            /// <summary>Copies from rhs to lhs.</summary>
            /// <param name="lhs">Vector to copy into.</param>
            /// <param name="rhs">Vector to copy from.</param>
            /// <returns>Returns: vector4. vector4 is reference to lhs.</returns>
            inline vector4& copy(vector4& lhs, const vector4& rhs) noexcept {
                std::copy(rhs.mData, rhs.mData + VECTOR4_LENGTH, lhs.mData);
                return lhs;
            }

            /// <summary>Adds two vectors and produces the third as a result.
            ///          add_new, new is from new vector as a result.</summary>
            /// <param name="lhs">Left hand side vector.</param>
            /// <param name="rhs">Right hand side vector.</param>
            /// <returns>Returns: vector4. New vector4 is result of addition.</returns>
            inline vector4 add_new(const vector4& lhs, const vector4& rhs) noexcept {
                vector4 result;
                // Load lhs into reg, load rhs into reg, add them up and store result in result variable.
                // _mm_store_ps(result.mData, _mm_add_ps(_mm_load_ps(lhs.mData), _mm_load_ps(rhs.mData)));
                array::add_array_4(result.mData, lhs.mData, rhs.mData);
                return result;
            }

            /// <summary>Adds two vectors, result is stored in lhs, rhs is unchanged.</summary>
            /// <param name="lhs">Left hand side vector.</param>
            /// <param name="rhs">Right hand side vector.</param>
            /// <returns>Returns: vector4. vector4 is reference to lhs.</returns>
            inline vector4& add(vector4& lhs, const vector4& rhs) noexcept {
                // Load lhs into reg, load rhs into reg, add them up and store result in lhs variable.
                // _mm_store_ps(lhs.mData, _mm_add_ps(_mm_load_ps(lhs.mData), _mm_load_ps(rhs.mData)));
                array::add_array_4(lhs.mData, lhs.mData, rhs.mData);
                return lhs;
            }

            /// <summary>Substracts two vectors and produces the third as a result.
            ///           substract_new, new is from new vector as a result.</summary>
            /// <param name="lhs">Left hand side vector.</param>
            /// <param name="rhs">Right hand side vector.</param>
            /// <returns>Returns: vector4. New vector4 is result of substraction.</returns>
            inline vector4 substract_new(const vector4& lhs, const vector4& rhs) noexcept {
                vector4 result;
                // Load lhs into reg, load rhs into reg, substract them and store result in result variable.
                // _mm_store_ps(result.mData, _mm_sub_ps(_mm_load_ps(lhs.mData), _mm_load_ps(rhs.mData)));
                array::sub_array_4(result.mData, lhs.mData, rhs.mData);
                return result;
            }

            /// <summary>Substracts two vectors, result is stored in lhs, rhs is unchanged.</summary>
            /// <param name="lhs">Left hand side vector.</param>
            /// <param name="rhs">Right hand side vector.</param>
            /// <returns>Returns: vector4. vector4 is reference to lhs.</returns>
            inline vector4& substract(vector4& lhs, const vector4& rhs) noexcept {
                // Load lhs into reg, load rhs into reg, substract them and store result in lhs variable.
                // _mm_store_ps(lhs.mData, _mm_sub_ps(_mm_load_ps(lhs.mData), _mm_load_ps(rhs.mData)));
                array::sub_array_4(lhs.mData, lhs.mData, rhs.mData);
                return lhs;
            }

            /// <summary>Calculate vector magnitude.</summary>
            /// <param name="lhs">Input vector.</param>
            /// <returns>Float value, magnitude.</returns>
            inline float magnitude(const vector4& lhs) noexcept {
                // Load vector into __m128 var.
                __m128 vectorData = _mm_load_ps(lhs.mData);

                // square input variables and store result into squared variable
                vector4 squared;
                _mm_store_ps(squared.mData, _mm_mul_ps(vectorData, vectorData));
                return sqrt(squared.mData[0] + squared.mData[1] + squared.mData[2] + squared.mData[3]);
            }

            /// <summary>Compare vectors with tolerance.</summary>
            /// <param name="lhs">Left hand side vector.</param>
            /// <param name="rhs">Right hand side vector.</param>
            /// <param name="tolerance">Tolerance vector. Contains tolerance for each component.</param>
            /// <returns>Returns: bool. Indicating if two vectors are withing tolerance range.</returns>
            inline bool equal_with_tolerance(const vector4& lhs, const vector4& rhs, const vector4& tolerance) noexcept {
                // TODO. Implement.
                return false;
            }
        }
    }
}
