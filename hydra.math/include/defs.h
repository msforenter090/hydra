#pragma once

/// <summary>
/// Platform independent macro definistions for API export and memory alignment.
/// </summary>

#if defined _MSC_VER
////////////////////////////////////////////
//           Microsoft compiler           //
////////////////////////////////////////////

// Export macro
#if defined HYDRA_MATH_API_EXPORT
#define HYDRA_MATH_API __declspec(dllexport)
#else
#define HYDRA_MATH_API __declspec(dllimport)
#endif

// Memory alignment macro
#define align(x) __declspec(align(x))

#elif defined(__GNUC__) || defined(__GNUG__)
////////////////////////////////////////////
//                GCC / G++               //
////////////////////////////////////////////

// Export macro
#define HYDRA_MATH_API

// Memory alignment macro
#define align(x) __attribute__((aligned(x)))
#endif