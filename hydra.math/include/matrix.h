#pragma once

#include "defs.h"

namespace hydra {
    namespace math {
        namespace matrix {
            /// <summary>
            /// 4x4 matrix .Matrix is 16 byte aligned.
            /// </summary>
            constexpr unsigned int MATRIX4x4_LENGTH = 16;
            using matrix4x4 = struct align(16) __matrix4x4 {
                float mData[16];
            };
        }
    }
}
