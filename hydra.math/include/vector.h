#pragma once

#include "defs.h"

namespace hydra {
    namespace math {
        namespace vector {
            /// <summary>
            /// Vector of length 4 with 16 bytes memory aligned.
            /// </summary>
            constexpr unsigned int VECTOR4_LENGTH = 4;
            using vector4 = struct align(16) __vector4 {
                // x, y, z, w
                float mData[4];
            };
        }
    }
}
