# HYDRA #

Hydra is a cross platform game engine written using c++.

### What is this repository for? ###

* This repository holds all of the libraries and source code
used in this project.

### How do I get set up? ###

Code can be build for Windows 10 and Ubunty 16.04.
NOTE: It is not tested for other platforms. 

To build the engine CMake must be used.
On Windows Visual Studio 15 and NMake are supported on Ubuntu Makefiles is supported.

### Windows Build ###
Create "bin" folder next (in same level as top level CMake file) to CMakeLists.txt file.
Navigate terminal to bin folder and execute:

* cmake ..                                                              for Visual Studio files 32 bit
* cmake -G "Visual Studio 14 2015 Win64" ..                             for Visual Studio files 64 bit
* cmake -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=[Release|Debug] ..      for NMake files

If generated Visual Studio files use Visual Studio to build project.
If generated NMake files execute "nmake all" to build project.

### Ubuntu Build ###
Create "bin" folder next (in same level as top level CMake file) to CMakeLists.txt file.
Navigate terminal to bin folder and execute:

* cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..                           for Makefiles