#include "relative_path.h"

#include <deque>
#include <sstream>

// string trimming
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

// directory / file detection
#include <sys/types.h>
#include <sys/stat.h>

// regex matching
#include <regex>

// Replaces platform dependent path separator for platform independent one.
#include "separator.h"

// path and path segment extraction regex
static const std::string path_delimiter("/");
static const std::regex path_regex("([a-zA-Z0-9-+_]+(\\.[a-zA-Z0-9]+)*\\/?)*");
static const std::regex segment_regex("[a-zA-Z0-9-+\\._]+\\/?");

/// <summary>PIMPL Implementation Start</summary>
class hydra::file_system::relative_path::relative_path_implementation {
public:
    std::deque<std::string> path_segments;
};
/// <summary>PIMPL Implementation End</summary>

/// <summary>Trim input string from start.</summary>
inline std::string& left_trim(std::string& input) noexcept {
    input.erase(input.begin(), std::find_if(input.begin(), input.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return input;
}

/// <summary>Trim input string from end.</summary>
inline std::string& right_trim(std::string& input) noexcept {
    input.erase(std::find_if(input.rbegin(), input.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), input.end());
    return input;
}

/// <summary>Trim input.</summary>
inline std::string& trim(std::string& input) {
    return left_trim(right_trim(input));
}

void hydra::file_system::relative_path::push_path_delimiter() noexcept {
    // if stack is empty, nowhere to push
    if(implementation->path_segments.size() == 0) {
        return;
    }

    std::string& segment = implementation->path_segments.back();
    if(segment.find(path_delimiter) != std::string::npos) {
        // There is path delimiter in segment.
        return;
    }

    // There is no path delimiter at last segment end, push it.
    segment.append(path_delimiter);
}

void hydra::file_system::relative_path::pop_path_delimiter() noexcept {
    if(implementation->path_segments.size() == 0) {
        return;
    }

    std::string& segment = implementation->path_segments.back();
    if(segment.find(path_delimiter) == std::string::npos) {
        // No path delimiter in segment.
        return;
    }

    // There is no path delimiter at last segment end, push it.
    segment.pop_back();
}

bool hydra::file_system::relative_path::is_segment_valid(const std::string& new_segment) const noexcept {
    std::smatch match;
    return std::regex_match(new_segment, match, segment_regex);
}

void hydra::file_system::relative_path::to_stream(std::ostream& out) const noexcept {
    if(implementation == nullptr) {
        return;
    }

    for(const std::string& segment : implementation->path_segments) {
        out << segment;
    }
}

hydra::file_system::relative_path::relative_path() noexcept : implementation(nullptr) {
    implementation = new relative_path_implementation();
}

hydra::file_system::relative_path::relative_path(const std::string& path) noexcept : relative_path() {
    try_parse(path);
}

hydra::file_system::relative_path::relative_path(const relative_path& rhs) noexcept : relative_path() {
    implementation->path_segments = rhs.implementation->path_segments;
}

hydra::file_system::relative_path::relative_path(relative_path&& rhs) noexcept {
    implementation = rhs.implementation;
    rhs.implementation = nullptr;
}

hydra::file_system::relative_path::~relative_path() {
    delete implementation;
    implementation = nullptr;
}

bool hydra::file_system::relative_path::operator==(const relative_path& rhs) noexcept {
    return implementation->path_segments == rhs.implementation->path_segments;
}

bool hydra::file_system::relative_path::operator!=(const relative_path& rhs) noexcept {
    return !(*this == rhs);
}

hydra::file_system::relative_path& hydra::file_system::relative_path::operator=(const relative_path& rhs) noexcept {
    // Check self assignment.
    if(this == &rhs)
        return *this;
    implementation->path_segments = rhs.implementation->path_segments;
    return *this;
}

hydra::file_system::relative_path& hydra::file_system::relative_path::operator=(relative_path&& rhs) noexcept {
    if(this == &rhs)
        return *this;
    delete implementation;
    implementation = rhs.implementation;
    rhs.implementation = nullptr;
    return *this;
}

hydra::file_system::relative_path hydra::file_system::operator+(const relative_path& lhs, const relative_path& rhs) noexcept {
    relative_path result{ lhs };
    result.push_path_delimiter();
    // TODO: Reduce code duplication betwean operator+ and operator+=
    // Copy entire rhs to result.
    result.implementation->path_segments.insert(
        result.implementation->path_segments.end(),
        rhs.implementation->path_segments.begin(),
        rhs.implementation->path_segments.end());
    return result;
}

hydra::file_system::relative_path& hydra::file_system::relative_path::operator+=(const relative_path& rhs) noexcept {
    // Copy entire rhs to result.
    push_path_delimiter();
    implementation->path_segments.insert(
        implementation->path_segments.end(),
        rhs.implementation->path_segments.begin(),
        rhs.implementation->path_segments.end());
    return *this;
}

std::ostream& hydra::file_system::operator<<(std::ostream& stream, const relative_path& rhs) noexcept {
    rhs.to_stream(stream);
    return stream;
}

bool hydra::file_system::relative_path::is_directory() const noexcept {
    if(implementation->path_segments.size() == 0) {
        return true;
    }
    bool ret = implementation->path_segments.back().back() == path_delimiter.back();
    return ret;
}

bool hydra::file_system::relative_path::is_file() const noexcept {
    return !is_directory();
}

bool hydra::file_system::relative_path::push_back_file(const std::string& file_name) noexcept {
    if(!is_segment_valid(file_name)) {
        return false;
    }

    push_path_delimiter();
    implementation->path_segments.push_back(file_name);
    pop_path_delimiter();
    return true;
}

bool hydra::file_system::relative_path::push_back_directory(const std::string& directory_name) noexcept {
    if(!push_back_file(directory_name)) {
        return false;
    }

    push_path_delimiter();
    return true;
}

std::string hydra::file_system::relative_path::pop_back_segment() noexcept {
    std::string return_value;
    if(implementation->path_segments.size() == 0) {
        return return_value;
    }

    return_value = implementation->path_segments.back();
    implementation->path_segments.pop_back();
    return return_value;
}

unsigned int hydra::file_system::relative_path::depth() const noexcept {
    return implementation->path_segments.size();
}

void hydra::file_system::relative_path::clear() noexcept {
    implementation->path_segments.clear();
}

bool hydra::file_system::relative_path::try_parse(const std::string path) noexcept {
    std::string input_path = path;
    trim(input_path);

    if(input_path.size() == 0) {
        // Empty string is acceptable. Empty string refers to current (working) directory.
        implementation->path_segments.clear();
        return true;
    }

    std::smatch base_match;
    if(!std::regex_match(input_path, base_match, path_regex)) {
        // Cannot parse given input path.
        return false;
    }

    std::deque<std::string> new_path;
    auto words_begin = std::sregex_iterator(input_path.begin(), input_path.end(), segment_regex);
    auto words_end = std::sregex_iterator();

    for(std::sregex_iterator i = words_begin; i != words_end; ++i) {
        std::smatch match = *i;
        std::string match_str = match.str();
        new_path.push_back(match_str);
    }

    implementation->path_segments = std::move(new_path);
    return true;
}

bool hydra::file_system::relative_path::exists() const noexcept {
    //struct stat info;
    //std::stringstream path_stream;
    //to_stream(path_stream);
    return false;
}
