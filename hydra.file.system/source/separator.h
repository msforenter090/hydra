#pragma once

#include <string>

// string replace
#include <algorithm>

namespace hydra {
    namespace file_system {
    #define WINDOWS_PATH_SEPARATOR "\\"
    #define UNIX_PATH_SEPARATOR "/"
    #define PLATFORM_INDEPENDENT_PATH_SEPARATOR "/"

        inline std::string platform_dependent_path_separator() {
        #ifdef _WIN32
            return WINDOWS_PATH_SEPARATOR;
        #elif defined __linux__
            return UNIX_PATH_SEPARATOR;
        #else
        #error Platform not defined.
        #endif // _WIN32
        }

        inline std::string platform_independent_path_separator() {
            return PLATFORM_INDEPENDENT_PATH_SEPARATOR;
        }

        inline void replace_platform_dependent_for_platform_independent_path_separator(std::string& input) {
            // From Windows device_path is expected to be like 'C:\\...\\...\\...', path separated with double backslashes.
            // Linux device_path is expected to be like '/../../.../...', with one forward slash as path delimiter.
            // In both cases path will end with program name and extension if exists.

            // This will be converted into platform independent path, using '/' as a delimiter.
            //std::replace(input.begin(), input.end(), platform_dependent_path_separator(), PLATFORM_INDEPENDENT_PATH_SEPARATOR);
            size_t pos = 0;
            std::string search = platform_dependent_path_separator();
            std::string replace = platform_independent_path_separator();
            while((pos = input.find(search, pos)) != std::string::npos) {
                input.replace(pos, search.length(), replace);
                pos += replace.length();
            }
        }

        inline void replace_platform_independent_for_platform_dependent_path_separator(std::string& input) {
            //std::replace(input.begin(), input.end(), PLATFORM_INDEPENDENT_PATH_SEPARATOR, platform_dependent_path_separator());
            size_t pos = 0;
            std::string search = platform_independent_path_separator();
            std::string replace = platform_dependent_path_separator();
            while((pos = input.find(search, pos)) != std::string::npos) {
                input.replace(pos, search.length(), replace);
                pos += replace.length();
            }
        }
    }
}