#include "device.h"

// Replaces platform dependent path separator for platform independent one.
#include "separator.h"

// Should be enought to hold almost any path.
#define MAX_PATH_LENGTH 4096

/// <summary>PIMPL Implementation Start</summary>
class hydra::file_system::device::device_implementation {
private:
    std::string executable;
    std::string device;

public:
    inline std::string& executable_name() {
        return executable;
    }

    inline std::string& device_path() {
        return device;
    }
};
/// <summary>PIMPL Implementation End</summary>

// _WIN32 is usually defined by compilers targeting 32 or 64 bit Windows systems
#ifdef _WIN32
#include <Windows.h>

/// <summary>Extracts program path and name.</summary>
std::string exe_path() noexcept {
    char result[MAX_PATH_LENGTH];
    return std::string(result, GetModuleFileName(nullptr, result, MAX_PATH_LENGTH));
}
#endif

#ifdef __linux__
#include <unistd.h>

/// <summary>Extracts program path and name.</summary>
std::string exe_path() noexcept {
    char result[MAX_PATH_LENGTH];

    // Read link returns -1 on fail, that is why we have '(count > 0) ? count : 0'
    // '>' is added becaue program must have a name, min one char.
    ssize_t count = readlink("/proc/self/exe", result, MAX_PATH_LENGTH);
    return std::string(result, (count > 0) ? count : 0);
}
#endif

void hydra::file_system::device::initialize_device() noexcept {
    implementation->device_path() = exe_path();
    replace_platform_dependent_for_platform_independent_path_separator(implementation->device_path());
    implementation->device_path().erase(implementation->device_path().find_last_of(implementation->executable_name()) - implementation->executable_name().size() + 1);
}

hydra::file_system::device::device(const std::string& executable_name) noexcept {
    implementation = new device_implementation();
    implementation->executable_name() = executable_name;
    initialize_device();
}

hydra::file_system::device::device(const device& rhs) noexcept {
    implementation = new device_implementation();
    implementation->device_path() = rhs.implementation->device_path();
    implementation->executable_name() = rhs.implementation->executable_name();
}

hydra::file_system::device::device(device&& rhs) noexcept {
    implementation = rhs.implementation;
    rhs.implementation = nullptr;
}

hydra::file_system::device::~device() noexcept {
    delete implementation;
    implementation = nullptr;
}

hydra::file_system::device& hydra::file_system::device::operator=(const device& rhs) noexcept {
    implementation->executable_name() = rhs.implementation->executable_name();
    implementation->device_path() = rhs.implementation->device_path();
    return *this;
}

hydra::file_system::device& hydra::file_system::device::operator=(device&& rhs) noexcept {
    if(&rhs == this)
        return *this;

    delete implementation;
    implementation = rhs.implementation;
    rhs.implementation = nullptr;
    return *this;
}

std::ostream& hydra::file_system::operator<<(std::ostream& out, const device& rhs) noexcept {
    out << rhs.implementation->device_path();
    return out;
}

unsigned int hydra::file_system::device::length() const noexcept {
    return implementation->device_path().length();
}

const std::string& hydra::file_system::device::device_path() const noexcept {
    return implementation->device_path();
}