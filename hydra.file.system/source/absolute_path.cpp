#include "absolute_path.h"

// stringstream
#include <sstream>

// Replaces platform dependent path separator for platform independent one.
#include "separator.h"

/// <summary>PIMPL Implementation Start</summary>
class hydra::file_system::absolute_path::absolute_path_implementation {
private:
    std::string unix_path;
    std::string windows_path;

public:
    inline std::string& upath() {
        return unix_path;
    }

    inline std::string& wpath() {
        return windows_path;
    }
};
/// <summary>PIMPL Implementation End</summary>

hydra::file_system::absolute_path::absolute_path(std::string& unix_path, std::string& windows_path) noexcept : implementation(nullptr) {
    implementation = new absolute_path_implementation();
    implementation->upath() = unix_path;
    implementation->wpath() = windows_path;
}

hydra::file_system::absolute_path::~absolute_path() {
    delete implementation;
    implementation = nullptr;
}

std::string hydra::file_system::absolute_path::path() {
#ifdef _WIN32
    return implementation->wpath();
#elif __linux__
    return implementation->upath();
#else
#error Platfom not defined.
#endif
}

std::string hydra::file_system::operator+(const device& lhs, const relative_path& rhs) noexcept {
    std::stringstream string_stream;
    string_stream << lhs << rhs;

    // lhs and rhs are platform independent paths, with platform independent delimiters.
    // Those delimiters should be changed to accomodate platform where code is running.
    std::string result_path = string_stream.str();
    replace_platform_independent_for_platform_dependent_path_separator(result_path);

    return result_path;
}

bool hydra::file_system::try_parse(const std::string& path, const device& device, relative_path& relative_path) noexcept {
    std::string input_copy = path;
    replace_platform_dependent_for_platform_independent_path_separator(input_copy);

    std::string device_string = device.device_path();

    // Remove device from path.
    std::string::size_type last = input_copy.find(device_string);
    if(last == std::string::npos) {
        return false;
    }

    input_copy.erase(0, last);
    return relative_path.try_parse(input_copy);
}
