#pragma once

/// <summary>
/// Platform independent macro definistions for API export and memory alignment.
/// </summary>

#if defined _MSC_VER
////////////////////////////////////////////
//           Microsoft compiler           //
////////////////////////////////////////////

// Export macro
#if defined HYDRA_FILE_SYSTEM_API_EXPORT
#define HYDRA_FILE_SYSTEM_API __declspec(dllexport)
#else
#define HYDRA_FILE_SYSTEM_API __declspec(dllimport)
#endif

#elif defined(__GNUC__) || defined(__GNUG__)
////////////////////////////////////////////
//                GCC / G++               //
////////////////////////////////////////////

// Export macro
#define HYDRA_FILE_SYSTEM_API
#endif