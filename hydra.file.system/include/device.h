#pragma once

#include "defs.h"

#include <string>

namespace hydra {
    namespace file_system {
        class device;
        HYDRA_FILE_SYSTEM_API std::ostream& operator<<(std::ostream& out, const device& rhs) noexcept;
    }
}

namespace hydra {
    namespace file_system {

        /// <summary>Used to resolve relative paths to absolute ones.</summary>
        class HYDRA_FILE_SYSTEM_API device {

        private:
            class device_implementation;
            device_implementation* implementation;

            void initialize_device() noexcept;

        public:
            /// <summary>Constructs a HD device using a given executable name.</summary>
            /// <param name="executable_name">Executable name.</param>
            explicit device(const std::string& executable_name) noexcept;

            /// <summary>Constructs new device from pre-existing one by copying old one. Original device is unchanged.</summary>
            /// <param name="rhs">Pre-existing device from which to construct new device.</param>
            device(const device& rhs) noexcept;

            /// <summary>Constructs new device from pre-existing one by moving old one to new.
            /// Original device is no longer valid and should not be used any more after move ctor.</summary>
            /// <param name="rhs">Pre-existing device from which to construct new device.</param>
            device(device&& rhs) noexcept;

            ~device() noexcept;

            /// <summary>Assign new value to existing device, old device is unchanged.</summary>
            /// <param name="rhs">Pre-existing device from which to assign new value.</param>
            device& operator=(const device& rhs) noexcept;

            /// <summary>Assign new value to existing device, old device is no longer valid.</summary>
            /// <param name="rhs">Pre-existing device from which to assign new value.</param>
            device& operator=(device&& rhs) noexcept;

            HYDRA_FILE_SYSTEM_API friend std::ostream& operator<<(std::ostream& out, const device& rhs) noexcept;

            /// <summary>Length of device path. Does not include null terminator.</summary>
            unsigned int length() const noexcept;

            /// <summary>Full path to executable without executable name.</summary>
            const std::string& device_path() const noexcept;
        };
    }
}
