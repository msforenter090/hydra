#pragma once

// TODO: Test operator assignment, move assignment.

#include <string>
#include <vector>

// Export macro
#include "defs.h"

namespace hydra {
    namespace file_system {
        class relative_path;
        HYDRA_FILE_SYSTEM_API relative_path operator+(const relative_path& lhs, const relative_path& rhs) noexcept;
        HYDRA_FILE_SYSTEM_API std::ostream& operator<<(std::ostream&, const relative_path& rhs) noexcept;
    }
}

namespace hydra {
    namespace file_system {

        /// <summary>Max length of path segment. Path segment is one step in full path.
        /// Example:
        ///         /home/milan/Documents/
        ///         home, milan and Documents are path segments.</summary>
        static constexpr unsigned int PATH_SEGMENT_LENGTH = 256;

        /// <summary>Represents relative path in a platform independent way.
        /// Acceptable carachters for segment are [a-zA-Z0-9-+]+[a-zA-Z0-9-+]*
        /// Independent platform delimiter is forward slash(/). 
        /// On windows path delimiter will be handled differently.</summary>
        class HYDRA_FILE_SYSTEM_API relative_path {

        private:
            class relative_path_implementation;
            relative_path_implementation* implementation;

            /// <summary>Makes a directory of current path.</summary>
            void push_path_delimiter() noexcept; 

            /// <summary>Pop path delimiter, removes path separator from end of the path if there is a path delimiter if no, there is no actiion.</summary>
            void pop_path_delimiter() noexcept;

            /// <summary>Check if given segment is valid in terms of format and characters.</summary>
            bool is_segment_valid(const std::string& new_segment) const noexcept;

            /// <summary>Serialize path to stream.</summary>
            void to_stream(std::ostream& out) const noexcept;

        public:
            /// <summary>Creates relative path to current directory.
            /// Contains '.' with depth of one.</summary>
            explicit relative_path() noexcept;

            /// <summary>Creates relative path to directory pointed by path.
            /// Parsing of the path can fail if path is not properly formated.
            /// In case where path is not properly formatted depth will return 1 (current directory).</summary>
            /// <param name="path">Relative path to folder or file.</param>
            relative_path(const std::string& path) noexcept;

            /// <summary>Creates a copy of the path.</symmary>
            /// <param name="rhs">relative path from which to create new path.</param>
            relative_path(const relative_path& rhs) noexcept;

            /// <summary>Move constructor.</symmary>
            /// <param name="rhs">relative path from which to move new path.</param>
            relative_path(relative_path&& rhs) noexcept;

            ~relative_path();

            /// <summary>Checks if paths are same.</symmary>
            /// <param name="rhs">relative path to compare with.</param>
            bool operator==(const relative_path& rhs) noexcept;

            /// <summary>Checks if paths are different.</symmary>
            /// <param name="rhs">relative path to compare with.</param>
            bool operator!=(const relative_path& rhs) noexcept;

            /// <summary>Assign new value to existing relative path, old relative path is unchanged.</summary>
            /// <param name="rhs">Pre-existing relative path from which to assign new value.</param>
            relative_path& operator=(const relative_path& rhs) noexcept;

            /// <summary>Assign new value to existing relative path, old relative path is no longer valid.</summary>
            /// <param name="rhs">Pre-existing relative path from which to assign new value.</param>
            relative_path& operator=(relative_path&& rhs) noexcept;

            /// <summary>Try to concatenate two relative paths. Producing new path.</symmary>
            /// <param name="lhs">First relative path to concatenate. This argument will come first in new relative path.</param>
            /// <param name="rhs">Secons relative path to concatenate with. This argument will come second in new relative path.</param>
            HYDRA_FILE_SYSTEM_API friend relative_path operator+(const relative_path& lhs, const relative_path& rhs) noexcept;

            /// <summary>Try to concatenate two relative paths. Updating lhs.</symmary>
            /// <param name="rhs">Relative path to concatenate with. This argument will come second in updated relative path.</param>
            relative_path& operator+=(const relative_path& rhs) noexcept;

            /// <summary>Serializes path in ostream.</symmary>
            /// <param name="stream">Stream to serialize in.</param>
            /// <param name="stream">Path to serialize.</param>
            HYDRA_FILE_SYSTEM_API friend std::ostream& operator<<(std::ostream& stream, const relative_path& rhs) noexcept;

            /// <summary>Checks if path is a directory.</symmary>
            bool is_directory() const noexcept;

            /// <summary>Checks if path is a file.</symmary>
            bool is_file() const noexcept;

            /// <summary>Adds new file name at the end of the file.</symmary>
            bool push_back_file(const std::string& file_name) noexcept;

            /// <summary>Adds new directory name at the end of the file.</symmary>
            bool push_back_directory(const std::string& directory_name) noexcept;

            /// <summary>Removes segment path bottom.</symmary>
            std::string pop_back_segment() noexcept;

            /// <summary>Depth of relative path.</symmary>
            unsigned int depth() const noexcept;

            /// <summary>Clear path segments. This is equivalent as calling ctor.</symmary>
            void clear() noexcept;

            /// <summary>Try to parse input path. Cannot start with '/'.</symmary>
            bool try_parse(const std::string path) noexcept;

            /// <summary>Checks if artifact exists on hard disk.</symmary>
            bool exists() const noexcept;
        };
    }
}
