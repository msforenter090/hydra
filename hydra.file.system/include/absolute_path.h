#pragma once

#include "defs.h"

#include "device.h"
#include "relative_path.h"

namespace hydra {
    namespace file_system {
        /// <summary>Concatenates device and relative path to produce apsolute path.
        /// Absolute path is platform dependent path.</summary>
        /// <param name="lhs">[In] Device prefix for relative path.</param>
        /// <param name="rhs">[In] Relative path as postfix to device.</param>
        /// <returns>Absolute platform dependent string to folder or file.</returns>
        HYDRA_FILE_SYSTEM_API std::string operator+(const device& lhs, const relative_path& rhs) noexcept;

        /// <summary>Try to extract relative path based on device and raw platform dependent path separator.</summary>
        /// <param name="path">[In] Raw input path with platform dependent path separator.</param>
        /// <param name="rhs">[In] Device to cut it off.</param>
        /// <param name="relative_path">[Out] Parsed relative path.</param>
        /// <returns>Returns true if parse was a success, false otherwise.</returns>
        HYDRA_FILE_SYSTEM_API bool try_parse(const std::string& path, const device& device, relative_path& relative_path) noexcept;
    }
}

namespace hydra {
    namespace file_system {
        class HYDRA_FILE_SYSTEM_API absolute_path {
        private:
            class absolute_path_implementation;
            absolute_path_implementation* implementation;

        public:
            absolute_path(std::string& unix_path, std::string& windows_path) noexcept;
            ~absolute_path();

            std::string path();
        };
    }
}