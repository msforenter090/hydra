#pragma once

#include "defs.h"
#include "hydra.platform.detection/type.h"

namespace hydra {
    namespace asn {
        class HYDRA_ASN_API byte_array {
        private:
            hydra::platform::type::uint32 mLength;
            hydra::platform::type::byte8* mData;

            inline void swap(byte_array& lhs, byte_array& rhs);
            inline void move(byte_array& lhs, byte_array&& rhs);

        public:

            /// <summary>Constructor will try to allocate ${length} bytes array.
            /// If allocation fails length is zero no exception will be thrown.
            /// </summary>
            explicit byte_array(const hydra::platform::type::uint32 length) noexcept;

            /// <summary>Copies ${length} bytes of data in new array.
            /// Constructor will try to allocate ${length} bytes for new array.
            /// If allocation fails length is zero. No exception will be thrown.
            /// </summary>
            explicit byte_array(const hydra::platform::type::uint32 length, const hydra::platform::type::byte8* const data) noexcept;

            /// <summary>Copies existing byte_array to new one.
            /// Constructor will try to allocate ${rhs.length} bytes for new array.
            /// If allocation fails length is zero. No exception will be thrown.
            /// </summary>
            byte_array(const byte_array& rhs) noexcept;

            /// <summary>Moves ${length} bytes of data to new array.</summary>
            explicit byte_array(byte_array&& rhs) noexcept;

            ~byte_array();

            /// <summary>Copies existing byte_array to new one.
            /// Assignment operator will try to allocate ${rhs.length} bytes for new array.
            /// If allocation fails length is zero. No exception will be thrown.
            /// </summary>
            byte_array& operator=(const byte_array& rhs) noexcept;

            /// <summary>Moves ${rhs.length} bytes of data to new array.</summary>
            byte_array& operator=(byte_array&& rhs) noexcept;

            inline hydra::platform::type::uint32 length() const noexcept { return mLength; }

            inline const hydra::platform::type::byte8* const data() const noexcept { return mData; }

            /// <summary>Returns reference to single byte in the array.
            /// Does not check for boundaries or if array is properly allocated.
            /// </summary>
            inline hydra::platform::type::byte8& operator[](const hydra::platform::type::uint32 index) noexcept { return mData[index]; }

            /// <summary>Returns const reference to single byte in the array.
            /// Does not check for boundaries or if array is properly allocated.
            /// </summary>
            inline const hydra::platform::type::byte8& operator[](const  hydra::platform::type::uint32 index) const noexcept { return mData[index]; }
            inline  hydra::platform::type::bool8 isAllocated() const noexcept { return mLength != 0; }
        };
    }
}