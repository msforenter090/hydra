#pragma once

/// <summary>
/// Platform independent macro definistions for API export and types.
/// </summary>

#if defined _MSC_VER
////////////////////////////////////////////
//           Microsoft compiler           //
////////////////////////////////////////////

// Export macro
#ifdef HYDRA_ASN_API_EXPORT
// If windows and no definistion of HYDRA_ASN_API_EXPORT
#define HYDRA_ASN_API __declspec(dllexport)
#else
#define HYDRA_ASN_API __declspec(dllimport)
#endif

#elif defined(__GNUC__) || defined(__GNUG__)
////////////////////////////////////////////
//                GCC / G++               //
////////////////////////////////////////////

// If not windows. It is linux
#define HYDRA_ASN_API
#endif