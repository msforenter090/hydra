#include "byte_array.h"

// malloc, free, std::move, std::swap
#include <algorithm>

// memset
#include <string.h>

hydra::asn::byte_array::byte_array(const hydra::platform::type::uint32 length) noexcept : mLength(0), mData(nullptr) {
    mData = (hydra::platform::type::byte8*)malloc(length * sizeof(hydra::platform::type::byte8));
    mLength = (mData != nullptr) * length;
    memset(mData, 0, mLength);
}

hydra::asn::byte_array::byte_array(const hydra::platform::type::uint32 length, const hydra::platform::type::byte8* const data) noexcept : byte_array(length) {
    if(mLength == 0) {
        return;
    }
    memcpy(mData, data, mLength);
}

hydra::asn::byte_array::byte_array(const byte_array& rhs) noexcept : byte_array(rhs.mLength, rhs.mData) {}

hydra::asn::byte_array::byte_array(byte_array&& rhs) noexcept {
    move(*this, std::move(rhs));
}

hydra::asn::byte_array::~byte_array() {
    mLength = 0;
    free(mData);
}

hydra::asn::byte_array& hydra::asn::byte_array::operator=(const byte_array& rhs) noexcept {
    byte_array other(rhs);

    // current content of the array will be transfered to 'other' object.
    // when other object goes out of scope it will be destroyed (freed).
    swap(*this, other);
    return *this;
}

hydra::asn::byte_array& hydra::asn::byte_array::operator=(byte_array&& rhs) noexcept {
    move(*this, std::move(rhs));
    return *this;
}

inline void hydra::asn::byte_array::swap(hydra::asn::byte_array& lhs, hydra::asn::byte_array& rhs) {
    std::swap(lhs.mLength, rhs.mLength);
    std::swap(lhs.mData, rhs.mData);
}

inline void hydra::asn::byte_array::move(hydra::asn::byte_array& lhs, hydra::asn::byte_array&& rhs) {
    // Move pointer and length to this instance
    lhs.mLength = rhs.mLength;
    lhs.mData = rhs.mData;

    // remove pointer and length from other instance
    rhs.mLength = 0;
    rhs.mData = nullptr;
}
