#pragma once

#include "gtest/gtest.h"

namespace hydra {
    namespace file_system {
        namespace test {
            class device_test : public ::testing::Test {
            protected:
                void SetUp() override;
                void TearDown() override;
            };
        }
    }
}