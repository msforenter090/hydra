#include "absolute_path_test.h"

#include "hydra.file.system/absolute_path.h"

namespace hydra {
    namespace file_system {
        namespace test {
            void apsolute_path_test::SetUp() {}
            void apsolute_path_test::TearDown() {}

            file_system::device get_device_object() {
            #ifdef _WIN32
                file_system::device device("test.hydra.file.system.exe");
            #endif

            #ifdef __linux__
                file_system::device device("test.hydra.file.system");
            #endif
                return device;
            }

            TEST_F(apsolute_path_test, operator_plus) {
                device dev = get_device_object();
                relative_path rel_path("+depth1.exe");

                std::string apsolute_path = dev + rel_path;
                std::cout << "apsolute_path_test, operator_plus: " << apsolute_path << std::endl;
                ASSERT_NE(0, apsolute_path.length());
            }

            TEST_F(apsolute_path_test, DISABLED_try_parse) {
                device dev = get_device_object();
                relative_path rel_path;

            #ifdef _WIN32
                std::string path("C:\\Users\\Milan\\Documents\\Visual Studio 2017\\Projects\\hydra\\output\\windows\\debug\\hydra.lib");
            #elif __linux__
                std::string path("/home/milan/Documents/projects/hydra/output/linux/debug/libhydra.so");
            #endif
                bool success = try_parse(path, dev, rel_path);
                ASSERT_TRUE(success);
            }
        }
    }
}