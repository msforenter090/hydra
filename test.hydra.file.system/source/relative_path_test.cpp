#include "relative_path_test.h"

#if _MSC_VER
#include <vld.h>
#endif

// stringstream
#include <sstream>

#include "hydra.file.system/relative_path.h"

namespace hydra {
    namespace file_system {
        namespace test {
            void relative_path_test::SetUp() {}
            void relative_path_test::TearDown() {}

            // Correct paths used for testing.
            constexpr unsigned int CORRECT_PATH_LENGTH = 14;
            std::string correct_paths[] = {
                std::string(""),
                std::string("depth1"),
                std::string("+depth1.exe"),
                std::string("+depth1.exe/"),
                std::string("depth1/"),
                std::string("depth1.exe.exe"),
                std::string("depth1/depth2/depth3/"),
                std::string("depth1/depth2/depth3"),
                std::string("depth1/depth2/depth3.elf"),
                std::string("depth1/depth2/depth3.elf/"),
                std::string("depth1/+depth2/+depth3.elf"),
                std::string("+depth1/depth2/depth3.elf"),
                std::string("-depth1/depth2/depth3.elf"),
                std::string("depth1/depth2/depth3.elf/")
            };

            // Correct paths used for testing.
            constexpr unsigned int FILE_PATH_LENGTH = 9;
            std::string file_paths[] = {
                std::string("depth1"),
                std::string("+depth1.exe"),
                std::string("depth1.exe.exe"),
                std::string("depth1/depth2/depth3"),
                std::string("depth1/depth2/depth3.elf"),
                std::string("depth1/+depth2/+depth3.elf"),
                std::string("+depth1/depth2/depth3.elf"),
                std::string("-depth1/depth2/depth3.elf"),
                std::string("depth1/depth2/depth3.elf")
            };

            // Correct paths used for testing.
            constexpr unsigned int DIRECTORY_PATH_LENGTH = 9;
            std::string directory_paths[] = {
                std::string("depth1/"),
                std::string("+depth1.exe/"),
                std::string("depth1.exe.exe/"),
                std::string("depth1/depth2/depth3/"),
                std::string("depth1/depth2/depth3.elf/"),
                std::string("depth1/+depth2/+depth3.elf/"),
                std::string("+depth1/depth2/depth3.elf/"),
                std::string("-depth1/depth2/depth3.elf/"),
                std::string("depth1/depth2/depth3.elf/")
            };


            // Incorrect paths used for testing.
            constexpr unsigned int INCORRECT_PATH_LENGTH = 7;
            std::string incorrect_paths[] = {
                std::string("?depth1"),
                std::string("?depth1/"),
                std::string("depth1//"),
                std::string("depth1?exe.exe"),
                std::string("depth1/depth2//"),
                std::string("depth1/depth2?/"),
                std::string("/depth1/depth2/depth3.elf/")
            };

            void check_path_and_depth(const unsigned int depth, const std::string string_path, const relative_path& path) {
                std::stringstream stream_path;
                stream_path << path;

                ASSERT_EQ(depth, path.depth()); 
                ASSERT_EQ(string_path, stream_path.str());
            }

            /// <summary>Test empty ctor.</summary>
            TEST_F(relative_path_test, empty_ctor) {
                relative_path path;
                check_path_and_depth(0, "", path);
            }

            /// <summary>Test ctor with correct path.</summary>
            TEST_F(relative_path_test, string_ctor_correct_paths) {
                for(unsigned int i = 0; i < CORRECT_PATH_LENGTH; ++i) {
                    std::stringstream sstream;
                    relative_path path{ correct_paths[i] };
                    sstream << path;
                    ASSERT_EQ(correct_paths[i], sstream.str());
                }
            }

            /// <summary>Test ctor with incorrect path.</summary>
            TEST_F(relative_path_test, string_ctor_incorrect_paths) {
                bool success_parsing = false;
                relative_path path{ correct_paths[10] };
                for(unsigned int i = 0; i < INCORRECT_PATH_LENGTH; ++i) {
                    std::stringstream sstream;
                    success_parsing = path.try_parse(incorrect_paths[i]);

                    sstream << path;

                    // Expected values is failed to parse paths.
                    ASSERT_FALSE(success_parsing);

                    // Expected value is last succesfull parsed path.
                    ASSERT_EQ(correct_paths[10], sstream.str());
                }
            }

            /// <summary>Test copy ctor.</summary>
            TEST_F(relative_path_test, copy_ctor) {
                for(unsigned int i = 0; i < CORRECT_PATH_LENGTH; ++i) {

                    // For each path make original and make a copy of it.
                    relative_path original{ correct_paths[i] };
                    relative_path copy{ original };

                    std::stringstream original_stream;
                    std::stringstream copy_stream;

                    original_stream << original;
                    copy_stream << copy;

                    // Check if two paths are equal by comparing their paths.
                    ASSERT_EQ(original_stream.str(), copy_stream.str());
                }
            }

            /// <summary>Test move ctor.</summary>
            TEST_F(relative_path_test, move_ctor) {
                for(unsigned int i = 0; i < CORRECT_PATH_LENGTH; ++i) {

                    // For each path make original and make a copy of it.
                    relative_path original{ correct_paths[i] };
                    relative_path copy{ std::move(original) };

                    std::stringstream original_stream;
                    std::stringstream copy_stream;

                    original_stream << original;
                    copy_stream << copy;

                    // Check if two paths are equal by comparing their paths.
                    ASSERT_EQ("", original_stream.str());
                    ASSERT_EQ(correct_paths[i], copy_stream.str());
                }
            }

            /// <summary>Test equal operator.</summary>
            TEST_F(relative_path_test, equal_operator) {
                for(unsigned int i = 0; i < CORRECT_PATH_LENGTH; ++i) {
                    relative_path lhs{ correct_paths[i] };
                    relative_path rhs{ correct_paths[i] };

                    ASSERT_TRUE(lhs == rhs);
                }
            }

            /// <summary>Test not equal operator.</summary>
            TEST_F(relative_path_test, not_equal_operator) {
                for(unsigned int i = 0; i < CORRECT_PATH_LENGTH - 1; ++i) {
                    relative_path lhs{ correct_paths[i] };
                    relative_path rhs{ correct_paths[i + 1] };

                    ASSERT_TRUE(lhs != rhs);
                }
            }

            /// <summary>Test assignment operator.</summary>
            TEST_F(relative_path_test, assignment_operator) {
                relative_path original(correct_paths[0]);
                relative_path copy(correct_paths[1]);

                // Original must differ from copy brefore assignment happens.
                ASSERT_TRUE(original != copy);

                copy = original;

                // After assignment original and copy must be same.
                ASSERT_TRUE(original == copy);
            }

            /// <summary>Test move assignment operator.</summary>
            TEST_F(relative_path_test, move_assignment_operator) {
                relative_path original(correct_paths[0]);
                relative_path copy(correct_paths[0]);
                relative_path move_path(correct_paths[1]);

                // Original must differ from move_path brefore assignment happens.
                ASSERT_TRUE(original != move_path);

                move_path = std::move(original);

                // After assignment original and copy must be same.
                ASSERT_TRUE(move_path == copy);

                // After move original should no longer be valid.
                std::stringstream stream;
                stream << original;
                ASSERT_EQ(0, stream.str().length());
            }

            /// <summary>Test both add operators.</summary>
            TEST_F(relative_path_test, add_operator) {
                relative_path lhs{ correct_paths[10] };
                relative_path rhs{ correct_paths[11] };

                relative_path expected_result("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf");
                relative_path result = lhs + rhs;
                std::stringstream string_stream;
                string_stream << result;
                ASSERT_TRUE(expected_result == result);

                lhs += rhs;
                ASSERT_TRUE(result == lhs);
            }

            /// <summary>Test is file method.</summary>
            TEST_F(relative_path_test, is_file) {
                relative_path path;
                for(unsigned int i = 0; i < FILE_PATH_LENGTH; ++i) {
                    ASSERT_TRUE(path.try_parse(file_paths[i]))      << "relative_path_test, is_file, try_parse: " << file_paths[i];
                    ASSERT_TRUE(path.is_file())                     << "relative_path_test, is_file, is_file: " << file_paths[i];
                    ASSERT_FALSE(path.is_directory())               << "relative_path_test, is_file, is_directory: " << file_paths[i];
                }
            }

            /// <summary>Test is_directory method.</summary>
            TEST_F(relative_path_test, is_directory) {
                relative_path path;
                for(unsigned int i = 0; i < DIRECTORY_PATH_LENGTH; ++i) {
                    ASSERT_TRUE(path.try_parse(directory_paths[i])) << "relative_path_test, is_file, try_parse: " << file_paths[i];
                    ASSERT_FALSE(path.is_file())                    << "relative_path_test, is_file, is_file: " << file_paths[i];
                    ASSERT_TRUE(path.is_directory())                << "relative_path_test, is_file, is_directory: " << file_paths[i];
                }
            }

            /// <summary>Test push_back_file method.</summary>
            TEST_F(relative_path_test, push_back_file) {
                relative_path lhs("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf");
                lhs.push_back_file("new_file");

                relative_path expected_result("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf/new_file");
                relative_path incorrect_result("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf/new_file/");
                ASSERT_TRUE(expected_result == lhs);
                ASSERT_FALSE(incorrect_result == lhs);
            }

            /// <summary>Test push_back_directory method.</summary>
            TEST_F(relative_path_test, push_back_directory) {
                relative_path lhs("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf");
                lhs.push_back_directory("new_directory");

                relative_path expected_result("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf/new_directory/");
                relative_path incorrect_result("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf/new_directory");
                ASSERT_TRUE(expected_result == lhs);
                ASSERT_FALSE(incorrect_result == lhs);
            }

            /// <summary>Test push_back_directory method.</summary>
            TEST_F(relative_path_test, pop_back_segment) {
                relative_path test_1;

                // Pop back segment from empty path should return empty string and keep path depth of zero.
                ASSERT_EQ(0, test_1.depth());
                std::string value = test_1.pop_back_segment();
                ASSERT_EQ("", value);
                ASSERT_EQ(0, test_1.depth());

                relative_path test_2("depth1/+depth2/+depth3.elf/+depth1/depth2/depth3.elf");
                ASSERT_EQ(6, test_2.depth());
                value = test_2.pop_back_segment();
                ASSERT_EQ("depth3.elf", value);
                ASSERT_EQ(5, test_2.depth());
                relative_path test_2_expected_result("depth1/+depth2/+depth3.elf/+depth1/depth2/");
                ASSERT_TRUE(test_2_expected_result == test_2);
            }
        }
    }
}
