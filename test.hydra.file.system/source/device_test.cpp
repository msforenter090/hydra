#include "device_test.h"

#include <sstream>

#if _MSC_VER
#include <vld.h>
#endif

#include "hydra.file.system/device.h"

#define MEX_LENGTH 512

namespace hydra {
    namespace file_system {
        namespace test {
            void device_test::SetUp() {}
            void device_test::TearDown() {}

            file_system::device get_device() {
            #ifdef _WIN32
                file_system::device device("test.hydra.file.system.exe");
            #endif

            #ifdef __linux__
                file_system::device device("test.hydra.file.system");
            #endif
                return device;
            }

            TEST_F(device_test, string_ctor) {
                file_system::device device = get_device();

                // Extract path and check if path is different from null.
                unsigned int length = device.length();
                std::string device_path = device.device_path();
                std::cout << "Loaded Path:" << device_path << std::endl;
                ASSERT_NE(0, length) << "device_test, empty_ctor, length is zero.";
            }

            TEST_F(device_test, copy_ctor) {
                file_system::device original = get_device();
                file_system::device copy(original);

                std::cout << "Copy Device:" << copy << std::endl;
            }

            TEST_F(device_test, move_ctor) {
                file_system::device original = get_device();
                file_system::device copy(std::move(original));

                std::cout << "Move Device:" << copy << std::endl;
            }

            TEST_F(device_test, move_assignment_operator) {

            }

            TEST_F(device_test, operator_ostream) {
                file_system::device device = get_device();
                std::stringstream stream;
                stream << device;
                std::cout << "Loaded Path:" << stream.str() << std::endl;
                ASSERT_NE(0, stream.str().length());
            }
        }
    }
}