#include "startup_socket.h"

#if defined _H_WINDOWS
#include <Winsock2.h>
bool8 hydra::network::startup_network_socket() noexcept {
    // Initialize latest version of client_socket.
    WSADATA wsa_data;
    return WSAStartup(MAKEWORD(2, 2), &wsa_data) == 0;
}
#elif defined _H_LINUX
bool8 hydra::network::startup_network_socket() noexcept {
    // Linux client_socket has no need for initialization.
    return true;
}
#else
#error Platform not defined.
#endif