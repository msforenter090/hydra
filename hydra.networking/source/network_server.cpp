#include "network_server.h"

#include "network_defs.h"

// ------------------------------------------------------------------------------------------------
// client socket serialization.
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/serialization.h"

// ------------------------------------------------------------------------------------------------
// std crossplatform formatting.
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/standard_system_calls.h"

// ------------------------------------------------------------------------------------------------
// close server socket
// ------------------------------------------------------------------------------------------------
#include "network_util.h"

// ------------------------------------------------------------------------------------------------
// PIMPL Implementation Start
// ------------------------------------------------------------------------------------------------
class hydra::network::network_server_implementation {
public:
    // --------------------------------------------------------------------------------------------
    // Holds address of the server, should hold localhost or 127.0.0.1 with port data.
    // --------------------------------------------------------------------------------------------
    struct addrinfo* _localhost;

    // --------------------------------------------------------------------------------------------
    // Platform indenendent socket descriptor.
    // --------------------------------------------------------------------------------------------
    _H_SOCKET _server_socket;

    network_server_implementation() : _localhost(nullptr), _server_socket(_H_INVALID_SOCKET) { }

    ~network_server_implementation() {
        // ----------------------------------------------------------------------------------------
        // Release allocated resources.
        // ----------------------------------------------------------------------------------------
        freeaddrinfo(_localhost);
        close_socket(_server_socket);
    }
};
// ------------------------------------------------------------------------------------------------
// PIMPL Implementation End.
// ------------------------------------------------------------------------------------------------

hydra::network::network_server::network_server(ip4_address& address,
    new_network_client_callback* new_client_callback,
    new_network_client_dropped_callback* new_clicent_droped_callback) noexcept 
    : _ip_address(address),
    _implementation(nullptr), _status(0x00),
    _new_client_callback(new_client_callback),
    _new_client_dropped_callback(new_clicent_droped_callback) {

    // --------------------------------------------------------------------------------------------
    // Do not throw if fails to allocate memory, return nullptr.
    // --------------------------------------------------------------------------------------------
    _implementation = new(std::nothrow) hydra::network::network_server_implementation();
}

hydra::network::network_server::network_server(network_server&& rhs) {
    // --------------------------------------------------------------------------------------------
    // Move imlementation to new network server.
    // --------------------------------------------------------------------------------------------
    _implementation = rhs._implementation;
    rhs._implementation = nullptr;
}

hydra::network::network_server::~network_server() {
    // --------------------------------------------------------------------------------------------
    // Release allocated resources.
    // --------------------------------------------------------------------------------------------
    delete _implementation;
    _implementation = nullptr;
}

bool8 hydra::network::network_server::available() const noexcept {
    return _implementation != nullptr;
}

bool8 hydra::network::network_server::obtain_address() noexcept {
    // --------------------------------------------------------------------------------------------
    // Implementations must be allocated before.
    // --------------------------------------------------------------------------------------------
    if(_implementation == nullptr)
        return false;

    // --------------------------------------------------------------------------------------------
    // Already successfully initialized, return true.
    // --------------------------------------------------------------------------------------------
    if((_status & 0x01) == 0x1)
        return true;

    // --------------------------------------------------------------------------------------------
    // Address is not obtained, last bit is zero.
    // --------------------------------------------------------------------------------------------
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = AI_PASSIVE;

    // --------------------------------------------------------------------------------------------
    // 0-65535 : 5 + 1, null terminator
    // --------------------------------------------------------------------------------------------
    constexpr uint32 port_buffer_length = 6;
    char8 string_port[port_buffer_length];
    hydra::platform::initialize_array(string_port, port_buffer_length);
    hydra::platform::itoa_f(_ip_address.port(), string_port, port_buffer_length);

    // --------------------------------------------------------------------------------------------
    // Success returns zero.
    // --------------------------------------------------------------------------------------------
    int32 result = getaddrinfo(NULL, string_port, &hints, &(_implementation->_localhost));
    if(result != 0) {
        _status &= 0xFE;
        return false;
    }

    // --------------------------------------------------------------------------------------------
    // Mark status bit as success.
    // --------------------------------------------------------------------------------------------
    _status |= 0x01;
    return true;
}

bool8 hydra::network::network_server::create_socket() noexcept {
    // --------------------------------------------------------------------------------------------
    // Implementations must be allocated before.
    // --------------------------------------------------------------------------------------------
    if(_implementation == nullptr)
        return false;

    // --------------------------------------------------------------------------------------------
    // Already successfully initialized, return true.
    // --------------------------------------------------------------------------------------------
    if((_status & 0x02) == 0x2)
        return true;

    // --------------------------------------------------------------------------------------------
    // Failure will return invalid socket.
    // --------------------------------------------------------------------------------------------
    _implementation->_server_socket = socket(
        _implementation->_localhost->ai_family,
        _implementation->_localhost->ai_socktype,
        _implementation->_localhost->ai_protocol);

    if(_implementation->_server_socket == _H_INVALID_SOCKET) {
        _status &= 0xFD;
        return false;
    }

    // --------------------------------------------------------------------------------------------
    // Mark status bit as success.
    // --------------------------------------------------------------------------------------------
    _status |= 0x02;
    return true;
}

bool8 hydra::network::network_server::bind_socket() noexcept {
    // --------------------------------------------------------------------------------------------
    // Implementations must be allocated before.
    // --------------------------------------------------------------------------------------------
    if(_implementation == nullptr)
        return false;

    // --------------------------------------------------------------------------------------------
    // Already successfully initialized, return true.
    // --------------------------------------------------------------------------------------------
    if((_status & 0x04) == 0x4)
        return true;

    // --------------------------------------------------------------------------------------------
    // If no error occurs, bind returns zero.
    // --------------------------------------------------------------------------------------------
    uint32 result = bind(
        _implementation->_server_socket,
        _implementation->_localhost->ai_addr,
        _implementation->_localhost->ai_addrlen);

    if(result != 0) {
        _status &= 0xFC;
        return false;
    }

    // --------------------------------------------------------------------------------------------
    // Mark status bit as success.
    // --------------------------------------------------------------------------------------------
    _status |= 0x04;
    return true;
}

void hydra::network::network_server::accept_clients() noexcept {
    // --------------------------------------------------------------------------------------------
    // Implementations must be allocated before.
    // --------------------------------------------------------------------------------------------
    if(_implementation == nullptr)
        return;

    if(_status != (0x01 | 0x02 | 0x04)) {
        // ----------------------------------------------------------------------------------------
        // Something is not initialized correctly, return.
        // ----------------------------------------------------------------------------------------
        return;
    }

    uint32 result = listen(_implementation->_server_socket, BACKLOG_SIZE);
    if(result != 0) {
        // ----------------------------------------------------------------------------------------
        // Drop the connection. Error occured. Report drop via callback.
        // ----------------------------------------------------------------------------------------
        (*_new_client_dropped_callback)();
        return;
    }

    // --------------------------------------------------------------------------------------------
    // Got new client, try to accept it.
    // --------------------------------------------------------------------------------------------
    struct sockaddr addr;
    memset(&addr, 0, sizeof(struct sockaddr));
    socklen_t addr_len = 0;
    _H_SOCKET client_socket = accept(_implementation->_server_socket, &addr, &addr_len);

    if(client_socket == _H_INVALID_SOCKET) {
        // ----------------------------------------------------------------------------------------
        // Drop the connection. Coult not accept client socket.
        // ----------------------------------------------------------------------------------------
        (*_new_client_dropped_callback)();
        return;
    }

    // --------------------------------------------------------------------------------------------
    // Serialize data needed for client_socket and create client socket, pass it to the callback.
    // For now serialization only involves docket descriptor.
    // --------------------------------------------------------------------------------------------
    constexpr uint32 serialization_data_length = sizeof(_H_SOCKET);
    ubyte8 byte_data[serialization_data_length];
    hydra::platform::serialize<uint32>(client_socket, byte_data, static_cast<uint32>(0));

    // --------------------------------------------------------------------------------------------
    // Safe to use move, we will no longer use client_socket after move.
    // --------------------------------------------------------------------------------------------
    hydra::network::client_socket client(byte_data, serialization_data_length);
    (*_new_client_callback)(std::move(client));
}