#pragma once

// --------------------------------------------------------------------------------
// _H_SOCKET
// --------------------------------------------------------------------------------
#include "network_defs.h"

// --------------------------------------------------------------------------------
// char8, void_ptr
// --------------------------------------------------------------------------------
#include "hydra.platform.detection/type.h"

using namespace hydra::platform::type;

namespace hydra {
    namespace network {

        // --------------------------------------------------------------------------------
        /// <summary>Handles closing of socket. Platform independent.</summary>
        /// <param name="socket_descriptor">[in] Socket descriptor.</param>
        // --------------------------------------------------------------------------------
        void close_socket(_H_SOCKET socket_descriptor) noexcept;

        // --------------------------------------------------------------------------------
        /// <summary>Converts presentation ip4 address to network based form.
        /// Platform independent.</summary>
        /// <param name="ip4_address">[in] Human readable ip4 address.</param>
        /// <param name="buffer">[out] Network bytes, output buffer.</param>
        /// <param name="buffer_length">[in] Length of .</param>
        // --------------------------------------------------------------------------------
        void presentation_ip4_to_network(char8* const ip4_address, void_ptr buffer, uint32 buffer_length) noexcept;
    }
}
