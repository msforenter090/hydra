#include "ip4_address.h"

// ------------------------------------------------------------------------------------------------
// Used to check input
// ------------------------------------------------------------------------------------------------
#include <regex>

// ------------------------------------------------------------------------------------------------
// Using: memset, string
// ------------------------------------------------------------------------------------------------
#include <string>

#include "hydra.platform.detection/standard_system_calls.h"

hydra::platform::type::char8 default_ip_address[] = { "127.0.0.1:8080\0" };

// ------------------------------------------------------------------------------------------------
// Regular expression to validate against client input.
// ------------------------------------------------------------------------------------------------
std::regex ip4_address_regex(
    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):"
    "(6553[0-5]|655[0-2]\\d|65[0-4]\\d\\d|6[0-4]\\d{3}|[1-5]\\d{4}|[1-9]\\d{0,3}|0)");

// ------------------------------------------------------------------------------------------------
// 4 * 3 + 3 + 1 + 5 + 1 = 21 + 1 = 21 + terminator character = 255.255.255.255:65535\0
// ------------------------------------------------------------------------------------------------
constexpr hydra::platform::type::uint32 max_input_length = 22;

using namespace hydra::platform::type;

hydra::network::ip4_address::ip4_address() noexcept : ip4_address(default_ip_address) {}

hydra::network::ip4_address::ip4_address(const platform::type::char8* const new_address) noexcept 
    : port_number(0) {

    // --------------------------------------------------------------------------------------------
    // Zero out the human readable arrays.
    // --------------------------------------------------------------------------------------------
    memset(human_readable_address, 0, ip4_address_max_length);

    // --------------------------------------------------------------------------------------------
    // Copy input. And work on copy.
    // --------------------------------------------------------------------------------------------
    char8 input[max_input_length];
    memcpy(input, new_address, strlen(new_address));
    input[strlen(new_address)] = '\0';

    std::smatch match;
    std::string string_input(input);
    if(!std::regex_match(string_input, match, ip4_address_regex)) {
        // Input does not match ip address reg ex, used default.
        memcpy(input, default_ip_address, strlen(default_ip_address));
        input[strlen(default_ip_address)] = '\0';
    }

    // --------------------------------------------------------------------------------------------
    // Null terminate input value, just in case.
    // --------------------------------------------------------------------------------------------
    input[max_input_length - 1] = '\0';

    // --------------------------------------------------------------------------------------------
    // Replace ':' with null terminating character.
    // First call to strtok will point to begin of the string a.k.a. ip address
    // and second call to strtok will point to port.
    // Colon will be replaced with null terminator character.
    // Copy first part to class member.
    // --------------------------------------------------------------------------------------------
    char8* token = hydra::platform::strtok_f(input, ":");
    memcpy(human_readable_address, token, strlen(token));

    // --------------------------------------------------------------------------------------------
    // Copy port to class member.
    // Exception should not occur. Input is validated against regular exception.
    // --------------------------------------------------------------------------------------------
    token = hydra::platform::strtok_f(nullptr, ":");
    port_number = atoi(token);
}