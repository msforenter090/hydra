#include "client_socket.h"

// ------------------------------------------------------------------------------------------------
// std::nothrow
// ------------------------------------------------------------------------------------------------
#include <new>

// ------------------------------------------------------------------------------------------------
// Using: _H_SOCKET
// ------------------------------------------------------------------------------------------------
#include "network_defs.h"
#include "network_util.h"
#include "hydra.platform.detection/serialization.h"

// ------------------------------------------------------------------------------------------------
// PIMPL Implementation Start
// ------------------------------------------------------------------------------------------------
namespace hydra {
    namespace network {
        void move(hydra::network::socket_implementation& dst, hydra::network::socket_implementation& src);
    }
}

class hydra::network::socket_implementation {
private:
    // --------------------------------------------------------------------------------------------
    // Platform independent socket descriptor.
    // --------------------------------------------------------------------------------------------
    _H_SOCKET _socket;

public:
    socket_implementation(_H_SOCKET socket) : _socket(socket) { }

    socket_implementation(socket_implementation&& rhs) {
        // ----------------------------------------------------------------------------------------
        // Move rhs to new socket_implementation.
        // rhs is invalid after this.
        // ----------------------------------------------------------------------------------------
        move(*this, rhs);
    }

    ~socket_implementation() {
        // ----------------------------------------------------------------------------------------
        // no need to handle nvalid socket, close_socket will handle it underneath
        // ----------------------------------------------------------------------------------------
        close_socket(_socket);
    }

    // --------------------------------------------------------------------------------------------
    /// <summary>Moves socket implementation from src to dst.</summary>
    /// <param name="dst">To where to move the socket implementation.</param>
    /// <param name="src">From where to move implementation. Src is invalid after moving.</param>
    // --------------------------------------------------------------------------------------------
    friend void move(hydra::network::socket_implementation& dst, hydra::network::socket_implementation& src);

    // --------------------------------------------------------------------------------------------
    // Forbiden methods. These methods are not allowed to be used or implemented.
    // --------------------------------------------------------------------------------------------
    socket_implementation(socket_implementation& rhs) = delete;
    socket_implementation& operator=(const socket_implementation& rhs) = delete;
    socket_implementation& operator=(const socket_implementation&& rhs) = delete;
};

void hydra::network::move(hydra::network::socket_implementation& dst,
    hydra::network::socket_implementation& src) {
    // --------------------------------------------------------------------------------------------
    // Close destination socket if there is one opened.
    // It is safe to pass invalid socket to close_socket method, function will handle it.
    // --------------------------------------------------------------------------------------------
    close_socket(dst._socket);

    // --------------------------------------------------------------------------------------------
    // Source socket descriptor ownership is moved to destination.
    // --------------------------------------------------------------------------------------------
    dst._socket = src._socket;

    // --------------------------------------------------------------------------------------------
    // Source socket now becomes invalid, we cannot have two client sockets descriptors
    // pointing at the same socket at the same time.
    // --------------------------------------------------------------------------------------------
    src._socket = _H_INVALID_SOCKET;
}
// ------------------------------------------------------------------------------------------------
// PIMPL Implementation End.
// ------------------------------------------------------------------------------------------------

hydra::network::client_socket::client_socket(const ubyte8* const data, uint32 length) noexcept 
    : _implementation(nullptr) {
    // --------------------------------------------------------------------------------------------
    // Do not throw if there is no enougth memory.
    // --------------------------------------------------------------------------------------------
    _implementation = new(std::nothrow)
        socket_implementation(hydra::platform::deserialize<uint32>(data, 0));
    // TODO: Finish deserialization.
}

hydra::network::client_socket::client_socket(client_socket&& rhs) {
    // --------------------------------------------------------------------------------------------
    // Move implementation to new client socket.
    // --------------------------------------------------------------------------------------------
    _implementation = rhs._implementation;

    // --------------------------------------------------------------------------------------------
    // Invalidate source socket. Source socket is no longer usable.
    // --------------------------------------------------------------------------------------------
    rhs._implementation = nullptr;
}

hydra::network::client_socket::~client_socket() {
    // --------------------------------------------------------------------------------------------
    // Clean up used resources.
    // --------------------------------------------------------------------------------------------
    delete _implementation;
    _implementation = nullptr;
}

bool8 hydra::network::client_socket::available() const noexcept {
    // --------------------------------------------------------------------------------------------
    // socket is valid for use if it memory is allocated.
    // --------------------------------------------------------------------------------------------
    return _implementation != nullptr;
}
