#include "network_message.h"

#include <algorithm>
#include <string.h>

hydra::network::network_message::network_message() noexcept : _length(0) {
    memset(_array, 0, NETWORK_ARRAY_MAX_LENGTH);
}

hydra::network::network_message::network_message(const uint32 start, const byte8* const new_array, const uint32 new_length) noexcept : network_message() {
    data(start, new_array, new_length);
}

void hydra::network::network_message::data(const uint32 start, const byte8* const new_array, const uint32 new_length) noexcept {
    memset(_array, 0, NETWORK_ARRAY_MAX_LENGTH);
    ushort16 length = std::min(new_length, NETWORK_ARRAY_MAX_LENGTH);
    memcpy(_array, new_array + start, length);
    _length = length;
}