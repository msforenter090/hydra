#pragma once

#include "defs.h"
#include "hydra.platform.detection/type.h"

using namespace hydra::platform::type;

namespace hydra {
    namespace network {
        bool8 shutdown_network_socket() noexcept;
    }
}