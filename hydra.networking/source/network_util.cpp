#include "network_util.h"

// ------------------------------------------------------------------------------------------------
// _H_WINDOWS, _H_LINUX
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/defs.h"

#if defined _H_WINDOWS

// ------------------------------------------------------------------------------------------------
// InetPton, close_socket
// ------------------------------------------------------------------------------------------------
#include <Ws2tcpip.h>
#elif defined _H_LINUX

// ------------------------------------------------------------------------------------------------
// strlen
// ------------------------------------------------------------------------------------------------
#include <string.h> 

// ------------------------------------------------------------------------------------------------
// inet_net_pton
// ------------------------------------------------------------------------------------------------
#include <arpa/inet.h>

// ------------------------------------------------------------------------------------------------
// close_socket
// ------------------------------------------------------------------------------------------------
#include <unistd.h>
#else
#error Platform not defined.
#endif

#define INET_PTON_SUCCESS 1
char8 DEFAULT_IP4_ADRESS[] = {"127.0.0.1\0"};

void hydra::network::close_socket(_H_SOCKET socket_descriptor) noexcept {
#if defined _H_WINDOWS
    closesocket(socket_descriptor);
#elif defined _H_LINUX
    close(socket_descriptor);
#else
#error Platform not defined.
#endif
}

void hydra::network::presentation_ip4_to_network(char8* const ip4_address, void_ptr buffer,
    uint32 buffer_length) noexcept {
#if defined _H_WINDOWS
    if(InetPton(AF_INET, ip4_address, buffer) != INET_PTON_SUCCESS) {
        // ----------------------------------------------------------------------------------------
        // This must be properly formatted address.
        // ----------------------------------------------------------------------------------------
        InetPton(AF_INET, DEFAULT_IP4_ADRESS, buffer);
    }
#elif defined _H_LINUX
    if(inet_net_pton(AF_INET, ip4_address, buffer, buffer_length) == -1) {
        // ----------------------------------------------------------------------------------------
        // This must be properly formatted address.
        // ----------------------------------------------------------------------------------------
        inet_net_pton(AF_INET, DEFAULT_IP4_ADRESS, buffer, buffer_length);
    }
#else
#error Platform not defined.
#endif
}
