#include "network_manager.h"

// ------------------------------------------------------------------------------------------------
// Two implementations handling start and shut down of socket for Windows and Linux.
// ------------------------------------------------------------------------------------------------
#include "startup_socket.h"
#include "shutdown_socket.h"

hydra::network::network_manager::network_manager() noexcept : _initialized(false) {
    _initialized = startup_network_socket();
}

hydra::network::network_manager::~network_manager() {
    // --------------------------------------------------------------------------------------------
    // Shut Down only if correctly initialized.
    // --------------------------------------------------------------------------------------------
    if(_initialized) {
        shutdown_network_socket();
    }
    _initialized = false;
}
