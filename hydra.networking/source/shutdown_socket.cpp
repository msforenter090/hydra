#include "shutdown_socket.h"

#if defined _H_WINDOWS
#include <Winsock2.h>
bool8 hydra::network::shutdown_network_socket() noexcept {
    return WSACleanup() == 0;
}
#elif defined _H_LINUX
bool8 hydra::network::shutdown_network_socket() noexcept {
    return true;
}
#else
#pragma error Platform not defined.
#endif
