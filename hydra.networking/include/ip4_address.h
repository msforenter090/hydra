// ------------------------------------------------------------------------------------------------
/// <summary> Used to store human readable ip v4 address. </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_NETWORKING_API
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// Platform idenpendent types.
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/type.h"

namespace hydra {
    namespace network {

        constexpr hydra::platform::type::ushort16 ip4_port_max_value = 65535;

        // ----------------------------------------------------------------------------------------
        // 4 * 3 + 3 + 1 = 15 + 1 = 15 + terminator string = 255.255.255.255\0
        // ----------------------------------------------------------------------------------------
        constexpr hydra::platform::type::uint32 ip4_address_max_length = 16;

        // ----------------------------------------------------------------------------------------
        /// <summary>Represent ip4 address with port.
        /// Port is delimited with ':' from ip address.
        /// Example of proper ip4 address: 193.168.1.52:7100</summary>
        // ----------------------------------------------------------------------------------------
        class HYDRA_NETWORKING_API ip4_address {
        private:
            // ------------------------------------------------------------------------------------
            // Network order address bytes.
            // ------------------------------------------------------------------------------------
            hydra::platform::type::ushort16 port_number;
            platform::type::char8 human_readable_address[ip4_address_max_length];

        public:
            // ------------------------------------------------------------------------------------
            /// <summary>Creates default ip address port pair.
            /// Default ip address is loop address(127.0.0.1:8080).</summary>
            // ------------------------------------------------------------------------------------
            ip4_address() noexcept;

            // ------------------------------------------------------------------------------------
            /// <summary>Creates new Ip4 address.
            /// Ip4Address will be created by copying max 21 character from input data.
            /// If input address is not properly formed address will point to localhost:8080.
            /// Max length: 255.255.255.255:65535 <-> 21 + 1 (null terminator).
            /// Must be null terminated.</summary>
            /// <param name="new_address">[in] Pointer to const char array containing the
            /// ip v4 address with port, delimited by ':'. Must be null terminated.</param>
            // ------------------------------------------------------------------------------------
            ip4_address(const platform::type::char8* const new_address) noexcept;

            // ------------------------------------------------------------------------------------
            /// <summary>Returns ip address in human readable form.
            /// string is null terminated.</summary>
            /// <returns>Returns const pointer to const data human readable ipv4 address.</returns>
            // ------------------------------------------------------------------------------------
            inline const platform::type::char8* const address() const noexcept {
                return human_readable_address;
            }

            // ------------------------------------------------------------------------------------
            /// <summary>Returns ip address in human readable form.
            /// string is null terminated.</summary>
            /// <returns>Returns copy of port number.</returns>
            // ------------------------------------------------------------------------------------
            inline hydra::platform::type::ushort16 port() const noexcept {
                return port_number;
           }
        };
    }
}