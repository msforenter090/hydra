// ------------------------------------------------------------------------------------------------
/// <summary>
/// Platform independent macro definistions.
/// Header provides library export macro.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: _H_WINDOWS, _H_LINUX
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/defs.h"

#if defined (_H_WINDOWS)
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Microsoft compiler                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// Export macro
#if defined HYDRA_NETWORKING_API_EXPORT
#define HYDRA_NETWORKING_API __declspec(dllexport)
#else
#define HYDRA_NETWORKING_API __declspec(dllimport)
#endif

#elif defined(_H_LINUX)
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                           GCC / G++                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// Export macro
#define HYDRA_NETWORKING_API

#endif