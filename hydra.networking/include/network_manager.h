// ------------------------------------------------------------------------------------------------
/// <summary>
/// Initializes and de initializes socket library.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_NETWORKING_API
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// Using: bool8
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/type.h"

using namespace hydra::platform::type;

namespace hydra {
    namespace network {
        class HYDRA_NETWORKING_API network_manager {
        private:
            bool8 _initialized;

        public:
            // ------------------------------------------------------------------------------------
            /// <summary>Initializes socket library.
            /// To verify initialization call 'initializaed()' function.</summary>
            // ------------------------------------------------------------------------------------
            network_manager() noexcept;

            // ------------------------------------------------------------------------------------
            // No copy, move, or any other operator. Just create instance of it and do not destroy
            // it untill network jobs are finished.
            // ------------------------------------------------------------------------------------
            network_manager(const network_manager&) = delete;
            network_manager(network_manager&&) = delete;

            ~network_manager();

            inline bool8 initialized() const noexcept {
                return _initialized;
            }

            network_manager& operator==(const network_manager&) = delete;
            network_manager& operator!=(network_manager&) = delete;

            network_manager& operator=(const network_manager&) = delete;
            network_manager& operator=(const network_manager&&) = delete;
        };
    }
}