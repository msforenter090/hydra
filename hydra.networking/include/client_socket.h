// ------------------------------------------------------------------------------------------------
/// <summary>
/// Socket client.
/// Produced by framework.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_PLATFORM_DETECTION_API
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_NETWORKING_API
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/type.h"

using namespace hydra::platform::type;

namespace hydra {
    namespace network {
        class socket_implementation;

        class HYDRA_NETWORKING_API client_socket {
        private:
            socket_implementation* _implementation;

        public:
            // ------------------------------------------------------------------------------------
            /// <summary>Creates new client socket by deserialazing input data.
            /// Users should not use this ctor manually.</summary>
            /// <param name="data">[in] Data from which to deserialize client socket.</param>
            /// <param name="length">[in] Length of the data.</param>
            // ------------------------------------------------------------------------------------
            client_socket(const ubyte8* const data, uint32 length) noexcept;

            // ------------------------------------------------------------------------------------
            /// <summary>Moves input client socket to current. Leaving input invalid.</summary>
            /// <param name="rhs">[in] client_socket from which to move from.</param>
            // ------------------------------------------------------------------------------------
            client_socket(client_socket&& rhs);

            ~client_socket();

            // ------------------------------------------------------------------------------------
            /// <summary>Check if client socket is correctly initialized.
            /// This should be done after construction has taken place.
            /// Client socket can be constructed and be invalid if no heap is left.</summary>
            /// <returns>Returns true if socket is available.</returns>
            // ------------------------------------------------------------------------------------
            bool8 available() const noexcept;

            // ------------------------------------------------------------------------------------
            // TODO: Implement Write, Read methods
            // ------------------------------------------------------------------------------------

            // ------------------------------------------------------------------------------------
            // Forbiden methods. These methods are not allowed to be used or implemented.
            // ------------------------------------------------------------------------------------
            client_socket(const client_socket& rhs) = delete;
            client_socket& operator=(const client_socket& rhs) = delete;
            client_socket& operator=(const client_socket&& rhs) = delete;
        };
    }
}