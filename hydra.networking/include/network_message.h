#pragma once

#include "defs.h"
#include "hydra.platform.detection/type.h"

using namespace hydra::platform::type;

namespace hydra {
    namespace network {
        static constexpr uint32 NETWORK_ARRAY_MAX_LENGTH = 256;

        class HYDRA_NETWORKING_API network_message {
        private:
            uint32 _length;
            byte8 _array[NETWORK_ARRAY_MAX_LENGTH];

        public:
            /// <summary>Constructs empty (length: 0), zeroout network message.</summary>
            explicit network_message() noexcept;

            /// <summary>Constructs new network message based on a provided input data.
            /// network message can take in max of ${NETWORK_ARRAY_MAX_LENGTH} bytes.</summary>
            /// <param name="start">Copy bytes starting from index ${start}.</param>
            /// <param name="new_array">Source from which to copy from.</param>
            /// <param name="new_length">Copy ${new_length} bytes from source into network_message.
            /// If ${new_length} is grater that ${NETWORK_ARRAY_MAX_LENGTH} it will be copied max ${NETWORK_ARRAY_MAX_LENGTH}.</param>
            explicit network_message(const uint32 start, const byte8* const new_array, const uint32 new_length) noexcept;

            /// <summary>Returns number of used bytes in the message.</summary>
            inline uint32 length() const noexcept {
                return _length;
            }

            /// <summary>Returns available bytes in the message.</summary>
            uint32 remaining() const noexcept {
                return NETWORK_ARRAY_MAX_LENGTH - _length;
            }

            /// <summary>Returns pointer to message data. Data cannot be edited or changed.</summary>
            const byte8* const data() const noexcept {
                return _array;
            }

            /// <summary>Sets message with new data. Same as ctor.</summary>
            void data(const uint32 start, const byte8* const new_array, const uint32 new_length) noexcept;
        };
    }
}