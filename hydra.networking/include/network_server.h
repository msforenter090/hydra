// ------------------------------------------------------------------------------------------------
/// <summary>
/// Accepts incoming clients and reports clients using passed callbacks.
/// Not threaded class. Threading should be provided to this class.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_NETWORKING_API
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// For function callback.
// ------------------------------------------------------------------------------------------------
#include <functional>

// ------------------------------------------------------------------------------------------------
// Type returned when new client is detected.
// ------------------------------------------------------------------------------------------------
#include "client_socket.h"

// ------------------------------------------------------------------------------------------------
// ip4_address for ctor input argument.
// ------------------------------------------------------------------------------------------------
#include "ip4_address.h"

// ------------------------------------------------------------------------------------------------
// uint32 for backlog size.
// ------------------------------------------------------------------------------------------------
#include "hydra.platform.detection/type.h"

using namespace hydra::network;
using namespace hydra::platform::type;

namespace hydra {
    namespace network {

        class network_server_implementation;

        // ----------------------------------------------------------------------------------------
        // Callback types.
        // ----------------------------------------------------------------------------------------
        using new_network_client_callback = std::function<void(client_socket client)>;
        using new_network_client_dropped_callback = std::function<void(void)>;

        // ----------------------------------------------------------------------------------------
        // Max number of pending connections.
        // ----------------------------------------------------------------------------------------
        constexpr uint32 BACKLOG_SIZE = 10;

        // ----------------------------------------------------------------------------------------
        /// <summary>Receives new client and reports to the subscriber using callback.</summary>
        // ----------------------------------------------------------------------------------------
        class HYDRA_NETWORKING_API network_server {
        private:
            // ------------------------------------------------------------------------------------
            // This should be localhost, since server cannot listen on other devices.
            // Only port is important and only port will be used from this ip_address.
            // ------------------------------------------------------------------------------------
            ip4_address _ip_address;

            network_server_implementation* _implementation;

            // ------------------------------------------------------------------------------------
            // Holds status of each network_server initialization step.
            // 0b00000000 - Uninitialized completely.
            // 0b00000001 - Obtained address.
            // 0b00000010 - Socket created.
            // 0b00000100 - Binded to port.
            // ------------------------------------------------------------------------------------
            char8 _status;

            new_network_client_callback* _new_client_callback;
            new_network_client_dropped_callback* _new_client_dropped_callback;

        public:
            // ------------------------------------------------------------------------------------
            /// <summary>Creates new server using passed in ip address and two callbacks.</summary>
            /// <param name="address">Server will listen on given port in ip address, ip part is
            /// ignored. Adress is copied internaly, original instance can be destroyed.</param>
            /// <param name="new_client_callback">Will be called when new client is detected.</param>
            /// <param name="new_clicent_droped_callback">Will be called when new client is dropped.
            /// (In case off error when receiving new user).</param>
            // ------------------------------------------------------------------------------------
            network_server(ip4_address& address,
                new_network_client_callback* new_client_callback,
                new_network_client_dropped_callback* new_clicent_droped_callback) noexcept;

            // ------------------------------------------------------------------------------------
            /// <summary>Moves network server to new instance making old one invalid for use.</summary>
            /// <param name="rhs">Old instance from where to move.</param>
            // ------------------------------------------------------------------------------------
            network_server(network_server&& rhs);

            ~network_server();

            // ------------------------------------------------------------------------------------
            /// <summary>Check if network server is correctly created.
            /// This should be called right after object construction.
            /// If method returns false there is no enought memory to create object neede internaly.
            /// </summary>
            // ------------------------------------------------------------------------------------
            bool8 available() const noexcept;

            // ------------------------------------------------------------------------------------
            // Series of method that must be called in given order to create server socket.
            // Each call bust be a success, it is safe to make multiple calls to one method.
            // ------------------------------------------------------------------------------------
            bool8 obtain_address() noexcept;
            bool8 create_socket() noexcept;
            bool8 bind_socket() noexcept;
            void accept_clients() noexcept;

            // ------------------------------------------------------------------------------------
            // Forbiden methods. These methods are not allowed to be used or implemented.
            // ------------------------------------------------------------------------------------
            network_server(const network_server&) = delete;
            network_server& operator=(const network_server&) = delete;
            network_server& operator=(const network_server&&) = delete;
        };
    }
}