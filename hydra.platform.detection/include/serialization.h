// ------------------------------------------------------------------------------------------------
/// <summary>
/// Serialization and deserialization utility for built in types.
/// This serialization should not be transfered across different machines, it is platform dependent
/// but fast.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Built in types.
// ------------------------------------------------------------------------------------------------
#include "type.h"

// ------------------------------------------------------------------------------------------------
// Using: UNUSED(...)
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// Using: memcpy(...)
// ------------------------------------------------------------------------------------------------
#include <memory.h>

namespace hydra {
    namespace platform {

        // ----------------------------------------------------------------------------------------
        /// <summary>Allow serialization / deserialization for enumerated types. Start.</summary>
        // ----------------------------------------------------------------------------------------
        template<typename type_name>
        struct type_serialization_rule;

        template<>
        struct type_serialization_rule<void> {};

        template<>
        struct type_serialization_rule<type::int32> {};

        template<>
        struct type_serialization_rule<type::uint32> {};

        template<>
        struct type_serialization_rule<type::int64> {};

        template<>
        struct type_serialization_rule<type::uint64> {};

        // ----------------------------------------------------------------------------------------
        /// <summary>Allow serialization / deserialization for enumerated types. End.</summary>
        // ----------------------------------------------------------------------------------------

        template<typename type_name, int length>
        union _serialization_deserialization_union {
            type_name type_value;
            type::ubyte8 buffer[length];
        };

        // ----------------------------------------------------------------------------------------
        /// <summary>Serialize type value.</summary>
        // ----------------------------------------------------------------------------------------
        template<typename type_name>
        void serialize(type_name type_value, type::ubyte8* const buffer,
            type::uint32 buffer_write_start) noexcept {
            // This will prevent serialization of unsuported types.
            struct type_serialization_rule<type_name> rule;
            UNUSED(rule);

            union _serialization_deserialization_union<type_name,
                sizeof(type_name)> serialize_union;
            serialize_union.type_value = type_value;
            memcpy(buffer + buffer_write_start, serialize_union.buffer, sizeof(type_name));
        }

        // ----------------------------------------------------------------------------------------
        /// <summary>Deserialize type value.</summary>
        // ----------------------------------------------------------------------------------------
        template<typename type_name>
        type_name deserialize(const type::ubyte8* const buffer,
            type::uint32 buffer_read_start) noexcept {
            // ------------------------------------------------------------------------------------
            // This will prevent deserialization of unsuported types.
            // ------------------------------------------------------------------------------------
            struct type_serialization_rule<type_name> rule;
            UNUSED(rule);

            union _serialization_deserialization_union<type_name,
                sizeof(type_name)> deserialize_union;
            memcpy(deserialize_union.buffer, buffer + buffer_read_start, sizeof(type_name));
            return deserialize_union.type_value;
        }
    }
}
