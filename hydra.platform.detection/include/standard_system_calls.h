// ------------------------------------------------------------------------------------------------
/// <summary>
/// Calls that differ from platform to platform, but does same thing.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_PLATFORM_DETECTION_API
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// Built in types (char8, int32, uint32).
// ------------------------------------------------------------------------------------------------
#include "type.h"

namespace hydra {
    namespace platform {

        HYDRA_PLATFORM_DETECTION_API type::char8* strtok_f(type::char8* token,
            const type::char8* delimit,
            type::char8** context = nullptr) noexcept;

        HYDRA_PLATFORM_DETECTION_API type::int32 itoa_f(
            type::int32 value,
            type::char8* buffer,
            type::uint32 buffer_size) noexcept;

        HYDRA_PLATFORM_DETECTION_API void initialize_array(type::char8* buffer,
            type::uint32 buffer_size) noexcept;
    }
}
