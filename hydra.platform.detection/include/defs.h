// ------------------------------------------------------------------------------------------------
/// <summary>
/// Platform independent macro definistions.
/// Header provides check macros for:
/// debug, release, bin32, bin64, windows, linux
/// for Microsoft compiler and GNU gcc / g++

/// When debug _H_DEBUG is defined.
/// When release _H_RELEASE is defined.
/// When target is 32bit _H_BIN32 is defined.
/// When target is 64bit _H_BIN64 is defined.
/// When windows _H_WINDOWS is defined.
/// When ubuntu _H_LINUX is defined.
/// </summary>
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Microsoft Compiler, min compiler version is 14.1 (Visual Studio 2017).
// ------------------------------------------------------------------------------------------------
#if defined _MSC_VER && _MSC_VER >= 1910
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Microsoft compiler                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------------------------------
// Platform types.
// ------------------------------------------------------------------------------------------------
#define _H_WINDOWS

// ------------------------------------------------------------------------------------------------
// Windows export macro.
// ------------------------------------------------------------------------------------------------
#ifdef HYDRA_PLATFORM_DETECTION_API_EXPORT
// If windows and no definistion of HYDRA_PLATFORM_DETECTION_API
#define HYDRA_PLATFORM_DETECTION_API __declspec(dllexport)
#else
#define HYDRA_PLATFORM_DETECTION_API __declspec(dllimport)
#endif

// ------------------------------------------------------------------------------------------------
// Define platform and build type Windows.
// ------------------------------------------------------------------------------------------------
#if defined _WIN32 && !defined _WIN64
#define _H_BIN32
#define _H_BUILD_SIZE 4
#endif

#if defined _WIN32 && defined _WIN64
#define _H_BIN64
#define _H_BUILD_SIZE 8
#endif

// GNU gcc / g++ compiler min version 6.0.0
#elif defined __GNUC__ && __GNUC__ >= 6 && \
    defined __linux__ || defined __linux || defined linux
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                           GCC / G++                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------------------------------
// Platform types.
// ------------------------------------------------------------------------------------------------
#define _H_LINUX

// ------------------------------------------------------------------------------------------------
// Linux export macro.
// ------------------------------------------------------------------------------------------------
#ifdef HYDRA_PLATFORM_DETECTION_API_EXPORT
// If windows and no definistion of HYDRA_PLATFORM_DETECTION_API
#define HYDRA_PLATFORM_DETECTION_API __attribute__((visibility("default")))
#else
#define HYDRA_PLATFORM_DETECTION_API
#endif

// ------------------------------------------------------------------------------------------------
// Define platform and build type Linux.
// ------------------------------------------------------------------------------------------------
#if defined __x86_64__ || defined __ppc64__
#define _H_BIN64
#define _H_BUILD_SIZE 8
#else
#define _H_BIN32
#define _H_BUILD_SIZE 4
#endif

#else 
#error "Unknown platform or min compiler version is not satisfied."
#endif

// ------------------------------------------------------------------------------------------------
// Common, regardles of compiler
// Debug, Release
// ------------------------------------------------------------------------------------------------
#ifdef NDEBUG
#define _H_RELEASE
#else
#define _H_DEBUG
#endif

// ------------------------------------------------------------------------------------------------
// Makes compiler stop complaining about unused variables and arguments.
// ------------------------------------------------------------------------------------------------
#define UNUSED(expr) do { (void)(expr); } while (0)