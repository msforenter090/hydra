// ------------------------------------------------------------------------------------------------
/// <summary>
/// Runtime checks for the platform.
/// Platform type, big, little endian, type sizes.
/// </summary> 
// ------------------------------------------------------------------------------------------------

#pragma once

// ------------------------------------------------------------------------------------------------
// Using: HYDRA_PLATFORM_DETECTION_API
// ------------------------------------------------------------------------------------------------
#include "defs.h"

// ------------------------------------------------------------------------------------------------
// Using: bool8
// ------------------------------------------------------------------------------------------------
#include "type.h"

namespace hydra {
    namespace platform {

        // ----------------------------------------------------------------------------------------
        /// <summary>Check if platform types have expected size.</summary>
        /// <returns>True if types sizes are expected.</returns>
        // ----------------------------------------------------------------------------------------
        HYDRA_PLATFORM_DETECTION_API type::bool8 check_platform_types() noexcept;

        // ----------------------------------------------------------------------------------------
        /// <summary>Return true if executing platform binaries are running on Windows.</summary>
        /// <returns>True if target is Windows.</returns>
        // ----------------------------------------------------------------------------------------
        HYDRA_PLATFORM_DETECTION_API type::bool8 windows() noexcept;

        // ----------------------------------------------------------------------------------------
        /// <summary>Return true if executing platform binaries are running on Ubuntu.</summary>
        /// <returns>True if target is Linux.</returns>
        // ----------------------------------------------------------------------------------------
        HYDRA_PLATFORM_DETECTION_API type::bool8 linux() noexcept;

        // ----------------------------------------------------------------------------------------
        /// <summary>Return true if running system is big endian.</summary>
        /// <returns>True if target is big endian.</returns>
        // ----------------------------------------------------------------------------------------
        HYDRA_PLATFORM_DETECTION_API type::bool8 big_endian() noexcept;
    }
}
