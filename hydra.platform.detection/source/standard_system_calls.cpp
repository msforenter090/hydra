#include "standard_system_calls.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

hydra::platform::type::char8* hydra::platform::strtok_f(type::char8* token,
    const type::char8* delimit, type::char8** context) noexcept {
#ifdef _H_WINDOWS
    return strtok(token, delimit);
#elif defined _H_LINUX
    return strtok(token, delimit);
#else
#pragma error Platform not defined.
#endif // _H_WINDOWS
}

hydra::platform::type::int32 hydra::platform::itoa_f(type::int32 value, type::char8* buffer,
    type::uint32 buffer_size) noexcept {
#ifdef _H_WINDOWS
    return _itoa_s(value, buffer, buffer_size, 10);
#elif defined _H_LINUX
    return sprintf(buffer,"%d", value);
#else
#pragma error Platform not defined.
#endif // _H_WINDOWS
}

void hydra::platform::initialize_array(type::char8* buffer, type::uint32 buffer_size) noexcept {
    memset(buffer, 0, buffer_size);
}

