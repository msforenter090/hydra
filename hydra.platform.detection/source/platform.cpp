#include "platform.h"

using namespace hydra::platform;

type::bool8 hydra::platform::check_platform_types() noexcept {
    return sizeof(type::bool8)  == 1 && 
        sizeof(type::byte8)     == 1 && sizeof(type::ubyte8)    == 1 &&
        sizeof(type::char8)     == 1 && sizeof(type::uchar8)    == 1 &&
        sizeof(type::short16)   == 2 && sizeof(type::ushort16)  == 2 &&
        sizeof(type::int32)     == 4 && sizeof(type::uint32)    == 4 &&
        sizeof(type::int64)     == 8 && sizeof(type::uint64)    == 8 &&
        sizeof(type::float32)   == 4 && sizeof(type::double64)  == 8 &&
        sizeof(type::auto_size_32_64) == _H_BUILD_SIZE &&
        sizeof(type::void_ptr) == _H_BUILD_SIZE;
}

type::bool8 hydra::platform::windows() noexcept {
#ifdef _H_WINDOWS
    return true;
#else
    return false;
#endif // _H_WINDOWS
}

type::bool8 hydra::platform::linux() noexcept {
    return !windows();
}

type::bool8 hydra::platform::big_endian() noexcept {
    union _endian_detection_union {
        type::uint32 int_value;
        type::char8 char_value[4];
    } endian_detection_union = {0x01};

    // --------------------------------------------------------------------------------------------
    // big endian representation    : 0x00 0x00 0x00 0x01
    // little endian representation : 0x01 0x00 0x00 0x00
    // --------------------------------------------------------------------------------------------
    return endian_detection_union.char_value[0] == 0x00;
}
